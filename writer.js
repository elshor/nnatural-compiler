//Tue, 15 Mar 2016 13:22:07 GMT
var ncore = require('nnatural/ncore');
function i(id,frame){if(frame[id]!== undefined){return frame[id];}ncore.getContextFrame(frame)[id] = 
require('nnatural/acore')['json']();return frame[id];}var v=ncore.nValue;
var local = {
	'isFrame' : true}
;
var thisModule = {
	'[] times []' : function(X, Y, frame){
	return require('nnatural/acore')['[] and [] and []'](require('nnatural/acore')['[] is integer'](X, frame), require('nnatural/acore')['[] is string'](Y, frame), require('nnatural/acore')['[] gte []'](X, 0, frame), frame)? function(x,y){var ret = ""; for(var i=0;i<v(X);++i){ret+=v(Y);}return ret;}(v(X),v(Y)) : require('nnatural/acore')['[] and []'](require('nnatural/acore')['[] is integer'](X, frame), require('nnatural/acore')['[] lte []'](X, 0, frame), frame)? "" : null;},
	'writer' : function(parent){
	var frame = ncore.Frame(null,null,local,'component');
	frame.this = frame;
	frame._events['open []'] = function(Path, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.Path = Path;
		return ncore.doAll([
			function(frame){return require('nnatural/file')['open file [] with path [] for write'](i('value',frame), i('Path', frame), frame, 'open file [] with path [] for write(node_modules/nnatural-compiler/writer:21)');}], frame, '*on open [](node_modules/nnatural-compiler/writer:20)');};
	frame._events['close'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/file')['close file []'](i('value',frame), frame, 'close file [](node_modules/nnatural-compiler/writer:23)');}], frame, '*on close(node_modules/nnatural-compiler/writer:22)');};
	frame._events['write []'] = function(X, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.X = X;
		return ncore.doAll([
			function(frame){return ncore.doWhen([
				[require('nnatural/acore')['[] gt []'](require('nnatural/acore')['length of []'](require('nnatural/acore')['[] as text'](i('X', frame), frame), frame), 0, frame), function(frame){return ncore.doAll([
					function(frame){return ncore.doWhen([
						[require('nnatural/acore')['not []'](i('after-beginning',frame), frame), function(frame){return ncore.doAll([
							function(frame){return ncore.send(i('this',frame), 'write indentation',[], frame, 'write indentation(node_modules/nnatural-compiler/writer:27)');}], frame, '*when not [](node_modules/nnatural-compiler/writer:26)');}],
						[true, function(frame){return ncore.doAll([
							function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/writer:29)');}], frame, '*otherwise(node_modules/nnatural-compiler/writer:28)');}]], frame);},
					function(frame){return require('nnatural/file')['write [] to []'](require('nnatural/acore')['[] as text'](i('X', frame), frame), i('value',frame), frame, 'write [] to [](node_modules/nnatural-compiler/writer:30)');},
					function(frame){return ncore.send(i('after-beginning',frame), '[]',[true], frame, '[](node_modules/nnatural-compiler/writer:31)');}], frame, '*when [] gt [](node_modules/nnatural-compiler/writer:25)');}],
				], frame);}], frame, '*on write [](node_modules/nnatural-compiler/writer:24)');};
	frame._events['writeln []'] = function(X, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.X = X;
		return ncore.doAll([
			function(frame){return ncore.doWhen([
				[require('nnatural/acore')['not []'](i('after-beginning',frame), frame), function(frame){return ncore.doAll([
					function(frame){return ncore.send(i('this',frame), 'write indentation',[], frame, 'write indentation(node_modules/nnatural-compiler/writer:34)');}], frame, '*when not [](node_modules/nnatural-compiler/writer:33)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/writer:36)');}], frame, '*otherwise(node_modules/nnatural-compiler/writer:35)');}]], frame);},
			function(frame){return require('nnatural/file')['write [] to []'](v([require('nnatural/acore')['[] as text'](i('X', frame), frame),"\n"]).join(''), i('value',frame), frame, 'write [] to [](node_modules/nnatural-compiler/writer:37)');},
			function(frame){return ncore.send(i('after-beginning',frame), '[]',[false], frame, '[](node_modules/nnatural-compiler/writer:38)');}], frame, '*on writeln [](node_modules/nnatural-compiler/writer:32)');};
	frame._events['write indentation'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/file')['write [] to []'](v([thisModule['[] times []'](i('indentation',frame), "\t", frame)]).join(''), i('value',frame), frame, 'write [] to [](node_modules/nnatural-compiler/writer:40)');}], frame, '*on write indentation(node_modules/nnatural-compiler/writer:39)');};
	frame._events['log indentation'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['log []'](v(["{",i('indentation',frame),"}"]).join(''), frame, 'log [](node_modules/nnatural-compiler/writer:42)');}], frame, '*on log indentation(node_modules/nnatural-compiler/writer:41)');};
	frame._events['indent'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['increment []'](i('indentation',frame), frame, 'increment [](node_modules/nnatural-compiler/writer:44)');}], frame, '*on indent(node_modules/nnatural-compiler/writer:43)');};
	frame._events['dedent'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['decrement []'](i('indentation',frame), frame, 'decrement [](node_modules/nnatural-compiler/writer:46)');}], frame, '*on dedent(node_modules/nnatural-compiler/writer:45)');};
	frame['indentation'] = require('nnatural/acore')['counter'](frame);
	frame['after-beginning'] = require('nnatural/acore')['json'](frame);
	frame['value'] = require('nnatural/acore')['json'](frame);
	return frame;},
	'append [] to []' : function(X, Str, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.X = X;
	frame.Str = Str;
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[i('X', frame),function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['set [] to []'](i('Str', frame), require('nnatural/acore')['[] pls []'](require('nnatural/acore')['[] default []'](i('Str', frame), "", frame), i('X', frame), frame), frame, 'set [] to [](node_modules/nnatural-compiler/writer:53)');}], frame, '*when [](node_modules/nnatural-compiler/writer:52)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/writer:55)');}], frame, '*otherwise(node_modules/nnatural-compiler/writer:54)');}]], frame);}], frame, '*append [] to [](node_modules/nnatural-compiler/writer:51)');},
	'string writer' : function(parent){
	var frame = ncore.Frame(null,null,local,'component');
	frame.this = frame;
	frame._events['write []'] = function(X, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.X = X;
		return ncore.doAll([
			function(frame){return ncore.doWhen([
				[i('X', frame),function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['not []'](i('value',frame), frame), function(frame){return ncore.doAll([
							function(frame){return ncore.send(i('value',frame), '[]',[""], frame, '[](node_modules/nnatural-compiler/writer:61)');}], frame, '*if not [](node_modules/nnatural-compiler/writer:60)');}],
						], frame);},
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](require('nnatural/acore')['length of []'](require('nnatural/acore')['[] as text'](i('X', frame), frame), frame), 0, frame), function(frame){return ncore.doAll([
							function(frame){return ncore.doIf([
								[require('nnatural/acore')['not []'](i('after-beginning',frame), frame), function(frame){return ncore.doAll([
									function(frame){return ncore.send(i('this',frame), 'write indentation',[], frame, 'write indentation(node_modules/nnatural-compiler/writer:64)');}], frame, '*if not [](node_modules/nnatural-compiler/writer:63)');}],
								], frame);},
							function(frame){return thisModule['append [] to []'](require('nnatural/acore')['[] as text'](i('X', frame), frame), i('value',frame), frame, 'append [] to [](node_modules/nnatural-compiler/writer:65)');},
							function(frame){return ncore.send(i('after-beginning',frame), '[]',[true], frame, '[](node_modules/nnatural-compiler/writer:66)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/writer:62)');}],
						], frame);}], frame, '*when [](node_modules/nnatural-compiler/writer:59)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/writer:68)');}], frame, '*otherwise(node_modules/nnatural-compiler/writer:67)');}]], frame);}], frame, '*on write [](node_modules/nnatural-compiler/writer:58)');};
	frame._events['writeln []'] = function(X, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.X = X;
		return ncore.doAll([
			function(frame){return ncore.doWhen([
				[i('X', frame),function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['not []'](i('value',frame), frame), function(frame){return ncore.doAll([
							function(frame){return ncore.send(i('value',frame), '[]',[""], frame, '[](node_modules/nnatural-compiler/writer:72)');}], frame, '*if not [](node_modules/nnatural-compiler/writer:71)');}],
						], frame);},
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['not []'](i('after-beginning',frame), frame), function(frame){return ncore.doAll([
							function(frame){return ncore.send(i('this',frame), 'write indentation',[], frame, 'write indentation(node_modules/nnatural-compiler/writer:74)');}], frame, '*if not [](node_modules/nnatural-compiler/writer:73)');}],
						], frame);},
					function(frame){return thisModule['append [] to []'](v([require('nnatural/acore')['[] as text'](i('X', frame), frame),"\n"]).join(''), i('value',frame), frame, 'append [] to [](node_modules/nnatural-compiler/writer:75)');},
					function(frame){return ncore.send(i('after-beginning',frame), '[]',[false], frame, '[](node_modules/nnatural-compiler/writer:76)');}], frame, '*when [](node_modules/nnatural-compiler/writer:70)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/writer:78)');}], frame, '*otherwise(node_modules/nnatural-compiler/writer:77)');}]], frame);}], frame, '*on writeln [](node_modules/nnatural-compiler/writer:69)');};
	frame._events['write indentation'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['not []'](i('value',frame), frame), function(frame){return ncore.doAll([
					function(frame){return ncore.send(i('value',frame), '[]',[""], frame, '[](node_modules/nnatural-compiler/writer:81)');}], frame, '*if not [](node_modules/nnatural-compiler/writer:80)');}],
				], frame);},
			function(frame){return thisModule['append [] to []'](v([thisModule['[] times []'](i('indentation',frame), "\t", frame)]).join(''), i('value',frame), frame, 'append [] to [](node_modules/nnatural-compiler/writer:82)');}], frame, '*on write indentation(node_modules/nnatural-compiler/writer:79)');};
	frame._events['log indentation'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['log []'](v(["{",i('indentation',frame),"}"]).join(''), frame, 'log [](node_modules/nnatural-compiler/writer:84)');}], frame, '*on log indentation(node_modules/nnatural-compiler/writer:83)');};
	frame._events['indent'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['increment []'](i('indentation',frame), frame, 'increment [](node_modules/nnatural-compiler/writer:86)');}], frame, '*on indent(node_modules/nnatural-compiler/writer:85)');};
	frame._events['dedent'] = function(parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['decrement []'](i('indentation',frame), frame, 'decrement [](node_modules/nnatural-compiler/writer:88)');}], frame, '*on dedent(node_modules/nnatural-compiler/writer:87)');};
	frame['indentation'] = require('nnatural/acore')['counter'](frame);
	frame['after-beginning'] = require('nnatural/acore')['json'](frame);
	frame['value'] = require('nnatural/acore')['json'](frame);
	return frame;},
	'writeln []' : function(X, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.X = X;
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.send(ncore.contextRef('stdout', frame), 'writeln []',[i('X', frame)], frame, 'writeln [](node_modules/nnatural-compiler/writer:94)');}], frame, '*writeln [](node_modules/nnatural-compiler/writer:93)');},
	'write []' : function(X, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.X = X;
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.send(ncore.contextRef('stdout', frame), 'write []',[i('X', frame)], frame, 'write [](node_modules/nnatural-compiler/writer:97)');}], frame, '*write [](node_modules/nnatural-compiler/writer:96)');},
	'indent' : function(parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.send(ncore.contextRef('stdout', frame), 'indent',[], frame, 'indent(node_modules/nnatural-compiler/writer:100)');}], frame, '*indent(node_modules/nnatural-compiler/writer:99)');},
	'dedent' : function(parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.send(ncore.contextRef('stdout', frame), 'dedent',[], frame, 'dedent(node_modules/nnatural-compiler/writer:103)');}], frame, '*dedent(node_modules/nnatural-compiler/writer:102)');}};
module.exports = thisModule;
