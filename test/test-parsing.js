//Fri, 29 May 2015 12:57:17 GMT
var ncore = require('nnatural/ncore');
function i(id,frame){if(frame[id]!== undefined){return frame[id];}ncore.getContextFrame(frame)[id] = 
require('nnatural/acore')['json']();return frame[id];}var v=ncore.nValue;
var local = {
	'isFrame' : true}
;
var thisModule = {
	'files in dir []' : function(Path, frame){
	return require('fs').readdirSync(v(Path));},
	'parse file []' : function(Name, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.Name = Name;
	frame['Source'] = require('nnatural/acore')['json'](frame);
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return require('nnatural/file')['load file []'](i('Name', frame), ncore.Frame(frame, {
				'[]' : function(File, parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler');
					frame._origin = origin;
					frame._loc = loc;
					frame.File = File;
					return ncore.doAll([
						function(frame){return ncore.send(i('Source', frame), '[]',[i('File', frame)], frame, '[](test-parsing:18)');}], frame, '*on [](test-parsing:17)');},
				'error []' : function(Error, parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler');
					frame._origin = origin;
					frame._loc = loc;
					frame.Error = Error;
					return ncore.doAll([
						function(frame){return require('nnatural/acore')['log []'](v(["Error loading file ",i('Name', frame)]).join(''), frame, 'log [](test-parsing:20)');},
						function(frame){return ncore.exitDoInOrder(frame, 'exit do in order(test-parsing:21)');}], frame, '*on error [](test-parsing:19)');}},frame), 'load file [](test-parsing:16)');},
			function(frame){return require('../compile-nnt')['parse nnt source []'](i('Source', frame), ncore.Frame(frame, {
				'[]' : function(Parsed, parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler');
					frame._origin = origin;
					frame._loc = loc;
					frame.Parsed = Parsed;
					return ncore.doAll([
						function(frame){return require('nnatural/file')['save [] as file named []'](require('nnatural/acore')['[] as text'](i('Parsed', frame), frame), v(["temp/",i('Name', frame),".json"]).join(''), frame, 'save [] as file named [](test-parsing:24)');},
						function(frame){return require('../compile-nnt')['get [] errors'](i('Parsed', frame), ncore.Frame(frame, {
							'parse error at [] text []' : function(Location, Line, parent, reportTo, origin, loc){
								var frame = ncore.Frame(reportTo, null, parent,'message-handler');
								frame._origin = origin;
								frame._loc = loc;
								frame.Location = Location;
								frame.Line = Line;
								return ncore.doAll([
									function(frame){return require('nnatural/acore')['log []'](v(["Parsing error in file ",i('Name', frame)," at ",ncore.PathRef(i('Location', frame), v(["start", "line"]))," - ",i('Line', frame)]).join(''), frame, 'log [](test-parsing:27)');}], frame, '*on parse error at [] text [](test-parsing:26)');}},frame), 'get [] errors(test-parsing:25)');},
						function(frame){return ncore.report(frame, '[]', [i('Parsed', frame)], '[](test-parsing:28)');}], frame, '*on [](test-parsing:23)');},
				'error [] parsing file [] at []' : function(Error, It, Pos, parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler');
					frame._origin = origin;
					frame._loc = loc;
					frame.Error = Error;
					frame.It = It;
					frame.Pos = Pos;
					return ncore.doAll([
						function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(test-parsing:30)');},
						function(frame){return require('nnatural/acore')['log []'](v(["Error parsing file ",i('Name', frame)," - ",i('Error', frame)]).join(''), frame, 'log [](test-parsing:31)');},
						function(frame){return ncore.report(frame, 'error []', [i('Error', frame)], 'error [](test-parsing:32)');}], frame, '*on error [] parsing file [] at [](test-parsing:29)');}},frame), 'parse nnt source [](test-parsing:22)');}], frame, 'do in order(test-parsing:15)');}], frame, '*parse file [](test-parsing:12)');},
	'main' : function(parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame['Successes'] = require('nnatural/acore')['counter'](frame);
	frame['Errors'] = require('nnatural/acore')['counter'](frame);
	frame._events['error []'] = function(Error, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.Error = Error;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['increment []'](i('Errors', frame), frame, 'increment [](test-parsing:40)');}], frame, '*on error [](test-parsing:39)');};
	frame._events['[]'] = function(Parsed, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.Parsed = Parsed;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['increment []'](i('Successes', frame), frame, 'increment [](test-parsing:42)');}], frame, '*on [](test-parsing:41)');};
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return ncore.doAll([
				function(frame){return ncore.forEach('File', thisModule['files in dir []']("scripts", frame), function(frame){return ncore.doAll([
					function(frame){return thisModule['parse file []'](v(["scripts/",i('File', frame)]).join(''), frame, 'parse file [](test-parsing:47)');}], frame, '*for each [] in files in dir [](test-parsing:45)');}, frame, '*for each [] in files in dir [](test-parsing:45)');}], frame, 'do all(test-parsing:44)');},
			function(frame){return require('nnatural/acore')['log []'](v(["number of successes: ",require('nnatural/acore')['[] default []'](i('Successes', frame), 0, frame)]).join(''), frame, 'log [](test-parsing:48)');},
			function(frame){return require('nnatural/acore')['log []'](v(["number of failures: ",require('nnatural/acore')['[] default []'](i('Errors', frame), 0, frame)]).join(''), frame, 'log [](test-parsing:49)');}], frame, 'do in order(test-parsing:43)');}], frame, '*main(test-parsing:35)');}};
module.exports = thisModule;
ncore.dispatch(module.exports.main,[ncore.isNEngineContext()? null : 
require('nnatural/acore')['shell'](null),null]);