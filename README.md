#nNatural Compiler
A compiler for the nNatural programming language, generating javascript node.js style modules.

## About nNatural
nNatural is a new computer language aiming to resemble as much as possible natural language such as English. Its resemblance to natural language makes it easier to understand, maintain and collaborate with, than existing computer languages. nNatural is *near* natural language and not natural language - it is a computer language with clear syntax and form that needs to be learned.

Read more about it at [http://github.com/nnatural/nnatural/readme.md]

## Writing nNatural programs
1. Install nNatural compiler
```
npm install -g nnatural-compiler
```

2. Install required javascript modules in nnatural source folder
```
npm insatll nnatural
```

3. Write the nNatural code using any text editor

4. Compile the code
```
nnt [my-nnatural-module]
```
5. Execute it
```
node [my-nnatural-module]
```

That's it. You're done.

## Next Steps
- Get a feel for nNatural code with a  [["learn nNatural in Y minutes" | examples/learn nnatural in Y minutes.nnt]] guide
- [[View nNatural language documentation | Language Topics]]
- Try out the [examples]
- [Send us your thoughts at nnaturallang@gmail.com](mailto:nnaturallang@gmail.com)
- Follow us on twitter - @nnaturallang