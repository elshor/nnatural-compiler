//Thu, 17 Mar 2016 15:02:47 GMT
var ncore = require('nnatural/ncore');
function i(id,frame){if(frame[id]!== undefined){return frame[id];}ncore.getContextFrame(frame)[id] = 
require('nnatural/acore')['json']();return frame[id];}var v=ncore.nValue;
var local = {
	'isFrame' : true}
;
var thisModule = {
	'generate type definition []' : function(Definition, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.Definition = Definition;
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('nnatural/acore')['set [] to []'](i('Type', frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Definition', frame), v(["statement", "name"])), "define type ", frame), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:18)');},
		function(frame){return ncore.forEach('Property', ncore.PathRef(i('Definition', frame), v(["children"])), function(frame){return ncore.doAll([
			function(frame){return ncore.doWhen([
				[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Property', frame), v(["statement", "name"])), "meta", frame), function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/generate-type:22)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/generate-type:20)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:25)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/generate-type:24)');}],
						], frame);},
					function(frame){return thisModule['generate property expression [] for type []'](i('Property', frame), i('Type', frame), frame, 'generate property expression [] for type [](node_modules/nnatural-compiler/generate-type:26)');},
					function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:27)');},
					function(frame){return thisModule['generate property definition [] getter for type []'](i('Property', frame), i('Type', frame), frame, 'generate property definition [] getter for type [](node_modules/nnatural-compiler/generate-type:28)');},
					function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:29)');},
					function(frame){return thisModule['generate property definition [] setter for type []'](i('Property', frame), i('Type', frame), frame, 'generate property definition [] setter for type [](node_modules/nnatural-compiler/generate-type:30)');}], frame, '*otherwise(node_modules/nnatural-compiler/generate-type:23)');}]], frame);}], frame, '*for each [] in [](node_modules/nnatural-compiler/generate-type:19)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/generate-type:19)');}], frame, '*generate type definition [](node_modules/nnatural-compiler/generate-type:16)');},
	'generate property definition [] getter for type []' : function(Property, Type, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.Property = Property;
	frame.Type = Type;
	frame['the context-type'] = "property";
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('nnatural/acore')['set [] to []'](i('PropertyName', frame), ncore.PathRef(i('Property', frame), v(["key", "name"])), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:36)');},
		function(frame){return require('nnatural/acore')['set [] to []'](i('PropertyType', frame), ncore.PathRef(i('Property', frame), v(["statement", "name"])), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:37)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:38)');},
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] gt []'](ncore.PathRef(require('./compile-nnt')['[] child block []'](i('Property', frame), "get", frame), v(["children", "length"])), 0, frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []']("function(Object,frame){", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:41)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:42)');},
				function(frame){return require('./writer')['writeln []']("return ncore.wrap(", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:43)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:44)');},
				function(frame){return require('./compile-nnt')['generate expression block []'](ncore.PathRef(require('./compile-nnt')['[] child block []'](i('Property', frame), "get", frame), v(["children"])), frame, 'generate expression block [](node_modules/nnatural-compiler/generate-type:45)');},
				function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:46)');},
				function(frame){return require('./writer')['writeln []'](v(["{$type:'",i('PropertyType', frame),"'});"]).join(''), frame, 'writeln [](node_modules/nnatural-compiler/generate-type:47)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/generate-type:48)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/generate-type:49)');},
				function(frame){return require('./writer')['write []']("}", frame, 'write [](node_modules/nnatural-compiler/generate-type:50)');}], frame, '*when [] gt [](node_modules/nnatural-compiler/generate-type:40)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []']("function(Object,parent){", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:52)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:53)');},
				function(frame){return require('./writer')['writeln []']("var frame = ncore.Frame(parent,null,parent,'context');", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:54)');},
				function(frame){return require('./writer')['writeln []']("frame._target = Object;", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:55)');},
				function(frame){return require('./writer')['writeln []'](v(["return g('",i('PropertyName', frame),"',Object,frame);"]).join(''), frame, 'writeln [](node_modules/nnatural-compiler/generate-type:56)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/generate-type:57)');},
				function(frame){return require('./writer')['write []']("}", frame, 'write [](node_modules/nnatural-compiler/generate-type:58)');}], frame, '*otherwise(node_modules/nnatural-compiler/generate-type:51)');}]], frame);},
		function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', ["expression", v(["__get ",i('PropertyName', frame)," of ",i('Type', frame)," []"]).join(''), "HAHA", ncore.contextRef('stdout', frame), i('Property', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/generate-type:59)');}], frame, '*generate property definition [] getter for type [](node_modules/nnatural-compiler/generate-type:32)');},
	'generate property definition [] setter for type []' : function(Property, Type, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.Property = Property;
	frame.Type = Type;
	frame['the context-type'] = "property";
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('nnatural/acore')['set [] to []'](i('PropertyName', frame), ncore.PathRef(i('Property', frame), v(["key", "name"])), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:65)');},
		function(frame){return require('nnatural/acore')['set [] to []'](i('PropertyType', frame), ncore.PathRef(i('Property', frame), v(["statement", "name"])), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:66)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:67)');},
		function(frame){return require('./writer')['writeln []']("function(Object,Value,parent,loc){", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:69)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:70)');},
		function(frame){return require('./writer')['writeln []']("var frame = ncore.Frame(parent,null,local,'action',loc);", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:71)');},
		function(frame){return require('./writer')['writeln []']("frame.Object = Object;", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:72)');},
		function(frame){return require('./writer')['writeln []']("frame.Value = Value;", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:73)');},
		function(frame){return require('./writer')['writeln []']("frame.this = frame;", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:74)');},
		function(frame){return require('./writer')['write []']("return ", frame, 'write [](node_modules/nnatural-compiler/generate-type:75)');},
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] gt []'](ncore.PathRef(require('./compile-nnt')['[] child block []'](i('Property', frame), "set", frame), v(["children", "length"])), 0, frame), function(frame){return ncore.doAll([
				function(frame){return require('./compile-nnt')['generate default dispatcher []'](require('./compile-nnt')['[] child block []'](i('Property', frame), "set", frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/generate-type:77)');}], frame, '*when [] gt [](node_modules/nnatural-compiler/generate-type:76)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['set [] to []'](i('Location', frame), v([ncore.contextRef('file-name', frame),":",ncore.PathRef(i('Property', frame), v(["location", "start", "line"]))]).join(''), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:80)');},
				function(frame){return require('./writer')['write []'](v(["ncore.doAll([function(frame){return s('",i('PropertyName', frame),"',Object,Value,frame,'default setter(",i('Location', frame),")');}],frame,'property ",i('PropertyName', frame),"(",i('Location', frame),")')"]).join(''), frame, 'write [](node_modules/nnatural-compiler/generate-type:81)');}], frame, '*otherwise(node_modules/nnatural-compiler/generate-type:78)');}]], frame);},
		function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/generate-type:82)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/generate-type:83)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/generate-type:84)');},
		function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', ["action", v(["__set ",i('PropertyName', frame)," of ",i('Type', frame)," [] to []"]).join(''), "DADA", ncore.contextRef('stdout', frame), i('Property', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/generate-type:85)');}], frame, '*generate property definition [] setter for type [](node_modules/nnatural-compiler/generate-type:61)');},
	'generate property expression [] for type []' : function(Property, Type, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame.Property = Property;
	frame.Type = Type;
	frame['the context-type'] = "property";
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('nnatural/acore')['set [] to []'](i('PropertyName', frame), ncore.PathRef(i('Property', frame), v(["key", "name"])), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:91)');},
		function(frame){return require('nnatural/acore')['set [] to []'](i('PropertyType', frame), ncore.PathRef(i('Property', frame), v(["statement", "name"])), frame, 'set [] to [](node_modules/nnatural-compiler/generate-type:92)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:93)');},
		function(frame){return require('./writer')['writeln []']("function(Container,parent){", frame, 'writeln [](node_modules/nnatural-compiler/generate-type:94)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/generate-type:95)');},
		function(frame){return require('./writer')['writeln []'](v(["return ncore.wrap(null,{$container: ncore.nWrapper(Container), $type:'",i('PropertyType', frame),"', $name:'",i('PropertyName', frame),"'});"]).join(''), frame, 'writeln [](node_modules/nnatural-compiler/generate-type:96)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/generate-type:97)');},
		function(frame){return require('./writer')['write []']("}", frame, 'write [](node_modules/nnatural-compiler/generate-type:98)');},
		function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', ["expression", v([i('PropertyName', frame)," of []"]).join(''), "HAHA", ncore.contextRef('stdout', frame), i('Property', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/generate-type:99)');}], frame, '*generate property expression [] for type [](node_modules/nnatural-compiler/generate-type:87)');}};
module.exports = thisModule;
