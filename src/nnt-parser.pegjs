//Copyright (C) 2015 Elchanan Shor. All rights reserved.
//This Source Code Form is subject to the terms of the Mozilla Public
//License, v. 2.0. If a copy of the MPL was not distributed with this
//file, You can obtain one at http://mozilla.org/MPL/2.0/.

start = l:line*{
	var top = function(arr){return arr[arr.length - 1];}
	var stack = [{"_type":"root","indent":-1,"children":[]}];
	for(var i=0;i<l.length;++i){
		if(l[i]._type === "empty-line"){
			continue;
		}
		while(stack.length > 1 && l[i].indent <= top(stack).indent){
			stack.pop();
		}
		if(!top(stack).children){
			top(stack).children = [];
		}
		top(stack).children.push(l[i]);
		stack.push(l[i]);
	}
	return stack[0];
}
line = block / remarkLine / wsLine / keyValueLine / freeTextLine

rstart = l:rline*{
	var top = function(arr){return arr[arr.length - 1];}
	var stack = [{"_type":"root","indent":-1,"children":[]}];
	for(var i=0;i<l.length;++i){
		if(l[i]._type === "empty-line"){
			continue;
		}
		while(stack.length > 1 && l[i].indent <= top(stack).indent){
			stack.pop();
		}
		if(!top(stack).children){
			top(stack).children = [];
		}
		top(stack).children.push(l[i]);
		stack.push(l[i]);
	}
	return stack[0];
}
rline = rblock / remarkLine / wsLine / keyValueLine / freeTextLine

block  = i:indent sp? key:(statement sp? ":" sp?)? target:(expression? sp? "<" sp?)? s:statement remark? (EOL / EOF){
	return {"_type":"block","text":text(),"statement":s,"indent":i,"location":location(),"children":[],"key":key? key[0] : 	null,"target":target? (target[0] || {"name":"_report"}): null}
}
rblock  = i:indent sp? s:rstatement remark? (EOL / EOF){
	return {"_type":"block","text":text(),"statement":s,"indent":i,"location":location(),"children":[],target:null}
}

indent = t:tab*{return t.length;}

tab = "\t"

//text    = c:[^\n^\r]+{ return c.join("") }

remarkLine = remark (EOL/EOF){return {"_type":"empty-line",location:location()}}

wsLine = ws (EOL/EOF){return {"_type":"empty-line",location:location()};}

freeTextLine = i:indent c:[^\n^\r]+ (EOL/EOF){return {"_type":"free-text","indent":i,text:c.join(""),location:location()};}

keyValueLine = i:indent ws? k:key ws? ":" ws? c:[^\n^\r]* (EOL/EOF){return {"_type":"key-value", "indent":i,key:k, value:c.join(""), location:location(),text:text()};}

key = k:([a-zA-Z0-9\-\/\\\ \*] / " ")+{return k.join("");}
EOF = !.

EOL = ("\r" / "\n")+

ws = (" " / "\t")+
rws = (" " / "\t")+{return {_type:'space',text:text()}}

remark = ws? "//" [^\n]*

sp = " "+

referenceExpression = "<" ws? r:referenceCharacter+ ns:namespaceSpecifier? ">" {
	return {name:"_reference",location:location(),ref:r.join(""),ns:ns||null};
}

referenceCharacter = [a-zA-Z0-9 " " "-" "_"]
namespaceElement = [^\n\(\)] / parenthesisElement
parenthesisElement = "(" namespaceElement* ")"{return text();}

namespaceCharacter = [a-z] / [A-Z] / [0-9] / "-" / " " / "/" / "\\" / "%" / "." / "#" / ":" / "*" / "@" / "$" / "\"" / "," / "'" / "[" / "]"
namespaceSpecifier = "(" n:namespaceElement* ")"{return n.join('');}
statement = h:statementElement t:(referenceExpression/ws statementElement)* ws? ns:namespaceSpecifier?{
	var ret = {"name":"","htmlName": "","text":text(),"args":[],"location":location()};
	var e = [h].concat(t? t : []);
	if(ns){
		ret.namespace = ns;
	}
	for(var i=0;i<e.length;++i){
		var current = Array.isArray(e[i])? e[i][1] : e[i];//e[i] is an array if ws exists --> take 2nd element
		if(Array.isArray(e[i])){
			//add back the space - only one space (we are normalizing spaces here)
			ret.name += " ";
			ret.htmlName += " ";
		}
		if(typeof current === "string"){
			ret.name += current;
			ret.htmlName += current;
		}else{
			ret.args.push(current);
			if(current.niceLabel){
				ret.htmlName += ("<span class='nnt-variable'>" + current.niceLabel + "</span>");
			}
			if(current.type){//this is a typed variable
				ret.name += ("[" + current.type.name + "]");
			}else if(current.ref){//this is a reference
				ret.name += ("[]"); //this is not a definitionStatement so we need the brackets empty
			}else{
				ret.name += "[]";
			}
		}
	}
	return ret;
}
//rstatement is a relaxed statement - enabling using upper-case and non-english characters as part of the statement text

rstatementCharacter = 
	[^[\n\{\"(\\< \]] / 
	"\\" ( "{" / "[" / "(" / "<" / "\\" ){return text()[1];}

rToken = x:rstatementCharacter+{return x.join("");}
rstatementElement = numberExpression / jsExpression / stringExpression / referenceExpression / rInlineExpression / rToken / rws

rstatement = e:rstatementElement+ ns:namespaceSpecifier?{
	var ret = {_type:"statement","elements":e,"name":"","text":text(),"args":[],"location":location()};
	if(ns){
		ret.namespace = ns;
	}
	for(var i=0;i<e.length;++i){
		if(typeof e[i] === "string"){
			ret.name += e[i];
		}else if(e[i]._type === 'space'){
			ret.name += " ";//normalizing spaces
		}else{
			ret.args.push(e[i]);
			ret.name += "[]";
		}
	}
	return ret;
}

//definition statement is used to define new entities (define action do something). It really has different
//behaviour. We ignored it until now but we have a very specific problem in that references should not emit
//[reference] but empty [] in regular statement. In definition we do need the specification
definitionStatement = h:statementElement t:(referenceExpression/ws statementElement)* ws? ns:namespaceSpecifier?{
	var ret = {"name":"","htmlName": "","text":text(),"args":[],"location":location()};
	var e = [h].concat(t? t : []);
	if(ns){
		ret.namespace = ns;
	}
	for(var i=0;i<e.length;++i){
		var current = Array.isArray(e[i])? e[i][1] : e[i];//e[i] is an array if ws exists --> take 2nd element
		if(Array.isArray(e[i])){
			//add back the space - only one space (we are normalizing spaces here)
			ret.name += " ";
			ret.htmlName += " ";
		}
		if(typeof current === "string"){
			ret.name += current;
			ret.htmlName += current;
		}else{
			ret.args.push(current);
			if(current.niceLabel){
				ret.htmlName += ("<span class='nnt-variable'>" + current.niceLabel + "</span>");
			}
			if(current.type){//this is a typed variable
				ret.name += ("[" + current.type.name + "]");
			}else if(current.ref){//this is a reference
				ret.name += ("[" + current.ref + "]");
			}else{
				ret.name += "[]";
			}
		}
	}
	return ret;
}

statementElement = e:(token / expression){return e;}

expression = expression3

expression0 = pathExpression / referenceExpression / jsExpression / inlineExpression / nullExpression / trueExpression / falseExpression / variableExpression / stringExpression / numberExpression / immediateExpression / contextExpression / templateExpression

expression1 = multExpression / divExpression / expression0

expression2 = plusExpression / minusExpression / expression1

expression3 = listExpression / expression2

plusExpression = x:expression1 sp? "+" sp? y:expression2 {return {"name":"[] pls []","args":[x,y]};}

minusExpression = x:expression1 sp? "-" sp? y:expression2 {return {"name":"[] minus []","args":[x,y]};}

multExpression = x:expression0 sp? "*" sp? y:expression1 {return {"name":"[] mul []","args":[x,y]};}

divExpression = x:expression0 sp? "/" sp? y:expression1  {return {"name":"[] div []","args":[x,y]};}

listExpression = h:expression2 sp? "," sp? t:expression3{return {"name":"_list", "args":[h].concat(t.name==="_list"?t.args : t),"location":location()};}

inlineExpression = "[" e:expression "]" {e.location = location();return e;} / 
	"[" s:definedExpression "]" {s.location=location();return s;}

rInlineExpression =	"[" d:"\\d"? s:rstatement "]" {return {"name":"_defined",text:text(),dispatch: d? 'defer' : 'normal',statement:s,location:location()};}

definedExpression = s:statement {return {"name":"_defined",text:text(),"statement":s, "location":location()};}

standaloneExpression = definedExpression / expression

typeExpression = "(" sp? type:statement sp? ")" sp? {return type;}

variableExpression = type:typeExpression? t:variableToken{return {"name":"_variable","niceLabel" : t.join(" "),"type":type,"args" : [t.join("")],"location":location()};}

capitalizedToken = h:uppercase t:(digit/lowercase)+{return text();}
uppercaseToken = h:uppercase uppercaseTokenRest{return text();}
uppercaseTokenRest = uppercase uppercaseTokenRest / uppercase !(lowercase/digit)
variableToken = t:(capitalizedToken/uppercaseToken/uppercase)+{return t;}

immediateExpression = "%" t:token{return {"name":"_immediate","args":[t],"location":location()};}

contextExpression = 
	"$" t:token{return {"name":"_context","args":[t]};} / 
	"$" t:variableExpression{return {"name":"_context","args":t.args,"location":location()};}

uppercase = [A-Z]

lowercase = [a-z]

digit = [0-9]

token = h:lowercase t:(lowercase/digit/"-")* {return h + t.join("");}

numberExpression = s:"-"? n:NumericLiteral{return {"location":location(),"name":"_number","args":[s? -n.value: n.value]};}

stringExpression = "\"" a:stringCharacter* "\"" {
	return {"location":location(),"name":"_string","args":[a?a.join(""):""]};} 

templateExpression = "\"" e:templateElement+ "\""{return {"location":location(),"name":"_template","args":e};}

trueExpression = "True" {return {"location":location(),"name":"_true"};}

nullExpression = "Null" {return {"location":location(),"name":"_null"};}

falseExpression = "False" {return {"location":location(),"name":"_false"};}

templateElement = templateInsert / templateText

templateInsert = "[[" e:expression "]]" {return e;} / "[[" e:definedExpression "]]" {return e;}

templateText = a:stringCharacter+ {return {"location":location(),"name":"_string", "args":[a.join("")]};}

stringCharacter = "\\\""{return "\\\"";} / !"[[" a:[^\"^\n]{return a;}

pathIdentifier = "." c:([a-zA-Z0-9]/ "-" / "_" / "$")+{return {"name":"_string","args":[c.join("")],"location":location()};}

pathElement = "."? e:inlineExpression {return e;} / pathIdentifier

//pathExpression = s:pathStart t:pathIdentifier+ {return {"name":"_path","args":[s,t],"location":location()};}

pathExpression = s:pathStart t:pathElement+ {return {"name":"_path","args":[s,t],"location":location()};}

pathStart = inlineExpression / variableExpression / immediateExpression / contextExpression / jsExpression

jsExpression = jsblock {return {"location":location(),"name":"_js","args":[text()]};}

jsblock = "{" ([^{}]/jsblock)* "}" {return text();}

//javascript NumericLiteral taken from https://github.com/pegjs/pegjs/blob/master/examples/javascript.pegjs adapting
//IdentifierStart to fit this context
IdentifierStart = [a-zA-Z] / "%" / "$"
NumericLiteral "number"
= literal:HexIntegerLiteral !(IdentifierStart / DecimalDigit) {
return literal;
}
/ literal:DecimalLiteral !(IdentifierStart / DecimalDigit) {
return literal;
}
DecimalLiteral
= DecimalIntegerLiteral "." DecimalDigit* ExponentPart? {
return { type: "Literal", value: parseFloat(text()) };
}
/ "." DecimalDigit+ ExponentPart? {
return { type: "Literal", value: parseFloat(text()) };
}
/ DecimalIntegerLiteral ExponentPart? {
return { type: "Literal", value: parseFloat(text()) };
}
DecimalIntegerLiteral
= "0"
/ NonZeroDigit DecimalDigit*
DecimalDigit
= [0-9]
NonZeroDigit
= [1-9]
ExponentPart
= ExponentIndicator SignedInteger
ExponentIndicator
= "e"i
SignedInteger
= [+-]? DecimalDigit+
HexIntegerLiteral
= "0x"i digits:$HexDigit+ {
return { type: "Literal", value: parseInt(digits, 16) };
}
HexDigit
= [0-9a-f]i
