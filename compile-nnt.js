//Thu, 22 Sep 2016 07:23:34 GMT
var ncore = require('nnatural/ncore');
function i(id,frame){if(frame[id]!== undefined){return frame[id];}ncore.getContextFrame(frame)[id] = require('nnatural/acore')['json']();return frame[id];}
var r=function(ref){return ref;};
var v=ncore.nValue;
var e=require('nnatural/context').emit;
var local = {
	'isFrame' : true}
;
var thisModule = {
	'argv' : 	function(frame){
		return ncore.waitValue([  ],function(){ return process.argv;});},
	'[] as uri encoded' : 	function(X, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ X ],function(){ return encodeURIComponent(v(X));});},
	'argument [] name' : 	function(Arg, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Arg ],function(){ return require('nnatural/acore')['[] starts with []'](ncore.PathRef(Arg, v(["ref"])), "a ", frame)? thisModule['[] with [] replacing all []'](require('nnatural/text')['[] after first []'](ncore.PathRef(Arg, v(["ref"])), "a ", frame), "_", " ", frame) : require('nnatural/acore')['[] starts with []'](ncore.PathRef(Arg, v(["ref"])), "an ", frame)? thisModule['[] with [] replacing all []'](require('nnatural/text')['[] after first []'](ncore.PathRef(Arg, v(["ref"])), "an ", frame), "_", " ", frame) : v(ncore.PathRef(Arg, v(["args", "0"])))? ncore.PathRef(Arg, v(["args", "0"])) : null;});},
	'[] is not action' : 	function(X, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ X ],function(){ return require('nnatural/acore')['[] eq []'](ncore.PathRef(X, v(["statement", "name"])), "defines", frame)? true : false;});},
	'warn []' : function(Message, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Message = Message;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('nnatural/acore')['log []'](i('Message', frame), frame, 'log [](node_modules/nnatural-compiler/compile-nnt:37)');}], frame, '*warn [](node_modules/nnatural-compiler/compile-nnt:36)');},
	'[] child block []' : 	function(Block, Name, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Block, Name ],function(){ return require('nnatural/acore')['not []'](Block, frame)? null : require('nnatural/acore')['[] and []'](require('nnatural/acore')['[] is an array'](Block, frame), require('nnatural/acore')['[] eq []'](ncore.PathRef(Block, v(["0", "statement", "name"])), Name, frame), frame)? ncore.PathRef(Block, v(["0"])) : require('nnatural/acore')['[] is an array'](Block, frame)? thisModule['[] child block []'](require('nnatural/acore')['[] tail'](Block, frame), Name, frame) : thisModule['[] child block []'](ncore.PathRef(Block, v(["children"])), Name, frame);});},
	'generate when block []' : function(List, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.List = List;
	frame['the Statement'] = ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement"]));
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "when ", frame), function(frame){return ncore.doAll([
				function(frame){return ncore.doWhen([
					[require('nnatural/acore')['[] eq []'](require('nnatural/acore')['[] after []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "when ", frame), "[]", frame), function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []']("[", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:54)');},
						function(frame){return thisModule['generate expression []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:55)');},
						function(frame){return require('./writer')['write []'](",function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:56)');},
						function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:57)');},
						function(frame){return require('./writer')['writeln []'](";}],", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:58)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:53)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []']("[", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:60)');},
						function(frame){return thisModule['generate resolved name [] namespace []'](require('nnatural/acore')['[] after []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "when ", frame), ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "namesapce"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:61)');},
						function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:62)');},
						function(frame){return ncore.forEach("Arg", ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args"])), function(frame){return ncore.doAll([
							function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:64)');},
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:65)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:63)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:63)');},
						function(frame){return require('./writer')['write []']("frame), function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:66)');},
						function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:67)');},
						function(frame){return require('./writer')['writeln []'](";}],", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:68)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:59)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:59');},
				function(frame){return thisModule['generate when block []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate when block [](node_modules/nnatural-compiler/compile-nnt:69)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:52)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "otherwise", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("[true, function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:71)');},
				function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:72)');},
				function(frame){return require('./writer')['write []'](ncore.template([";}]], frame,'@",ncore.contextRef('file-name', frame),":",ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "location", "start", "line"])),"');}"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:73)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:74)');},
				function(frame){return ncore.doWhen([
					[require('nnatural/acore')['[] tail'](i('List', frame), frame),function(frame){return ncore.doAll([
						function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:76)');},
						function(frame){return thisModule['generate action list []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:77)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:75)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:79)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:78)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:78');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:70)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["], frame,'@",ncore.contextRef('file-name', frame),":",ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["location", "start", "line"])),"');}"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:81)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:82)');},
				function(frame){return ncore.doWhen([
					[i('List', frame),function(frame){return ncore.doAll([
						function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:84)');},
						function(frame){return thisModule['generate action list []'](i('List', frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:85)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:83)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:87)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:86)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:86');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:80)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:80');}], frame, '*generate when block [](node_modules/nnatural-compiler/compile-nnt:49)');},
	'arguments [] list' : 	function(Arguments, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Arguments ],function(){ return require('nnatural/acore')['[] eq []'](ncore.PathRef(Arguments, v(["length"])), 0, frame)? "" : require('nnatural/acore')['[] eq []'](ncore.PathRef(Arguments, v(["length"])), 1, frame)? ncore.PathRef(Arguments, v(["0", "args", "0"])) : ncore.template([ncore.PathRef(Arguments, v(["0", "args", "0"])),", ",thisModule['arguments [] list'](require('nnatural/acore')['[] tail'](Arguments, frame), frame)]);});},
	'generate if block []' : function(List, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.List = List;
	frame['the Statement'] = ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement"]));
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "else if ", frame), function(frame){return ncore.doAll([
				function(frame){return ncore.doWhen([
					[require('nnatural/acore')['[] eq []'](require('nnatural/acore')['[] after []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "else if ", frame), "[]", frame), function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []']("[", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:102)');},
						function(frame){return thisModule['generate expression []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:103)');},
						function(frame){return require('./writer')['write []'](",function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:104)');},
						function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:105)');},
						function(frame){return require('./writer')['writeln []'](";}],", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:106)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:101)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []']("[", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:108)');},
						function(frame){return thisModule['generate resolved name [] namespace []'](require('nnatural/acore')['[] after []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "else if ", frame), ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:109)');},
						function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:110)');},
						function(frame){return ncore.forEach("Arg", ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["args"])), function(frame){return ncore.doAll([
							function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:112)');},
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:113)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:111)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:111)');},
						function(frame){return require('./writer')['write []']("frame), function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:114)');},
						function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:115)');},
						function(frame){return require('./writer')['writeln []'](";}],", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:116)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:107)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:107');},
				function(frame){return thisModule['generate if block []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate if block [](node_modules/nnatural-compiler/compile-nnt:117)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:100)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "else", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("[true, function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:119)');},
				function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:120)');},
				function(frame){return require('./writer')['write []'](";}]], frame);}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:121)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:122)');},
				function(frame){return ncore.doWhen([
					[require('nnatural/acore')['[] tail'](i('List', frame), frame),function(frame){return ncore.doAll([
						function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:124)');},
						function(frame){return thisModule['generate action list []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:125)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:123)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:127)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:126)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:126');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:118)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("], frame);}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:129)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:130)');},
				function(frame){return ncore.doWhen([
					[i('List', frame),function(frame){return ncore.doAll([
						function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:132)');},
						function(frame){return thisModule['generate action list []'](i('List', frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:133)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:131)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:135)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:134)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:134');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:128)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:128');}], frame, '*generate if block [](node_modules/nnatural-compiler/compile-nnt:97)');},
	'generate action call events frame []' : function(Defines, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Defines = Defines;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['writeln []']("ncore.Frame(frame, {", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:138)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:139)');},
		function(frame){return ncore.forEach("Define", i('Defines', frame), function(frame){return ncore.doAll([
			function(frame){return ncore.doWhen([
				[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), "on ", frame), function(frame){return ncore.doAll([
					function(frame){return ncore.doWhen([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:143)');}], frame, '*when [] gt [](node_modules/nnatural-compiler/compile-nnt:142)');}],
						[true, function(frame){return ncore.doAll([
							function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:145)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:144)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:144');},
					function(frame){return require('./writer')['write []'](ncore.template(["'",require('nnatural/acore')['[] after []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), "on ", frame),"' : "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:146)');},
					function(frame){return thisModule['generate handler []'](i('Define', frame), frame, 'generate handler [](node_modules/nnatural-compiler/compile-nnt:147)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:141)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:149)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:148)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:148');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:140)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:140)');},
		function(frame){return require('./writer')['write []'](ncore.template(["},frame,'handlers','*",ncore.contextRef('file-name', frame),":",ncore.PathRef(ncore.contextRef('Statement', frame), v(["location", "start", "line"])),"')"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:150)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:151)');}], frame, '*generate action call events frame [](node_modules/nnatural-compiler/compile-nnt:137)');},
	'generate action list []' : function(List, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.List = List;
	frame['the Statement'] = ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement"]));
	frame['the Role'] = "action";
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['not []'](ncore.contextRef('Statement', frame), frame), function(frame){return ncore.doAll([
				function(frame){return ncore.report(frame, 'parse error at []', [ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["location"]))], 'parse error at [](node_modules/nnatural-compiler/compile-nnt:158)');}], frame, '*when not [](node_modules/nnatural-compiler/compile-nnt:157)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('List', frame), v(["length"])), 0, frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['warn []']("got empty action list", frame, 'warn [](node_modules/nnatural-compiler/compile-nnt:160)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:159)');}],
			[thisModule['[] is not action'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate action list []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:162)');}], frame, '*when [] is not action(node_modules/nnatural-compiler/compile-nnt:161)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "use []", frame), function(frame){return ncore.doAll([
				function(frame){return ncore.doInOrder([
					function(frame){return ncore.send(ncore.contextRef('entity-resolver', frame), 'push library []',[ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args", "0", "args", "0"]))], frame, 'push library [](node_modules/nnatural-compiler/compile-nnt:165)');},
					function(frame){return thisModule['generate action list []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:166)');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:164)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:163)');}],
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "when ", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []']("function(frame){return ncore.doWhen([", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:168)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:169)');},
				function(frame){return thisModule['generate when block []'](i('List', frame), frame, 'generate when block [](node_modules/nnatural-compiler/compile-nnt:170)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:167)');}],
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "if ", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []']("function(frame){return ncore.doIf([", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:172)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:173)');},
				function(frame){return ncore.doWhen([
					[require('nnatural/acore')['[] eq []'](require('nnatural/acore')['[] after []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "if ", frame), "[]", frame), function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []']("[", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:175)');},
						function(frame){return thisModule['generate expression []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:176)');},
						function(frame){return require('./writer')['write []'](",function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:177)');},
						function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:178)');},
						function(frame){return require('./writer')['writeln []'](";}],", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:179)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:174)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []']("[", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:181)');},
						function(frame){return thisModule['generate resolved name [] namespace []'](require('nnatural/acore')['[] after []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "if ", frame), ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:182)');},
						function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:183)');},
						function(frame){return ncore.forEach("Arg", ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args"])), function(frame){return ncore.doAll([
							function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:185)');},
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:186)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:184)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:184)');},
						function(frame){return require('./writer')['write []']("frame), function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:187)');},
						function(frame){return thisModule['generate default dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:188)');},
						function(frame){return require('./writer')['writeln []'](";}],", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:189)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:180)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:180');},
				function(frame){return thisModule['generate if block []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate if block [](node_modules/nnatural-compiler/compile-nnt:190)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:171)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return ncore.doInOrder([
					function(frame){return ncore.doWhen([
						[require('nnatural/acore')['[] eq []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "do all", frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []']("function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:194)');},
							function(frame){return thisModule['generate do all dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate do all dispatcher [](node_modules/nnatural-compiler/compile-nnt:195)');},
							function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:196)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:193)');}],
						[require('nnatural/acore')['[] eq []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "do in order", frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []']("function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:198)');},
							function(frame){return thisModule['generate do in order dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate do in order dispatcher [](node_modules/nnatural-compiler/compile-nnt:199)');},
							function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:200)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:197)');}],
						[require('nnatural/acore')['[] starts with []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "for each [] in ", frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []']("function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:202)');},
							function(frame){return thisModule['generate for each dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate for each dispatcher [](node_modules/nnatural-compiler/compile-nnt:203)');},
							function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:204)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:201)');}],
						[require('nnatural/acore')['[] starts with []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "do in order for each [] in ", frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []']("function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:206)');},
							function(frame){return thisModule['generate do in order for each dispatcher []'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame, 'generate do in order for each dispatcher [](node_modules/nnatural-compiler/compile-nnt:207)');},
							function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:208)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:205)');}],
						[require('nnatural/acore')['[] eq []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), "exit do in order", frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](ncore.template(["function(frame){return ncore.exitDoInOrder(frame, '",thisModule['[] location in file []'](require('nnatural/acore')['[] head'](i('List', frame), frame), ncore.contextRef('file-name', frame), frame),"');}"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:210)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:209)');}],
						[thisModule['[] is send-report'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []']("function(frame){return ncore.report(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:212)');},
							function(frame){return ncore.doWhen([
								[require('nnatural/acore')['[] gt []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["children", "length"])), 0, frame), function(frame){return ncore.doAll([
									function(frame){return thisModule['generate action call events frame []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["children"])), frame, 'generate action call events frame [](node_modules/nnatural-compiler/compile-nnt:214)');}], frame, '*when [] gt [](node_modules/nnatural-compiler/compile-nnt:213)');}],
								[true, function(frame){return ncore.doAll([
									function(frame){return require('./writer')['write []']("frame", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:216)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:215)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:215');},
							function(frame){return require('./writer')['write []'](ncore.template([", '",ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])),"', ["]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:217)');},
							function(frame){return ncore.forEach("Arg", ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args"])), function(frame){return ncore.doAll([
								function(frame){return ncore.doIf([
									[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
										function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:220)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:219)');}],
									], frame);},
								function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:221)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:218)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:218)');},
							function(frame){return require('./writer')['write []'](ncore.template(["], '",thisModule['[] location in file []'](require('nnatural/acore')['[] head'](i('List', frame), frame), ncore.contextRef('file-name', frame), frame),"');}"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:222)');}], frame, '*when [] is send-report(node_modules/nnatural-compiler/compile-nnt:211)');}],
						[thisModule['[] is send-message'](require('nnatural/acore')['[] head'](i('List', frame), frame), frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []']("function(frame){return ncore.send(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:224)');},
							function(frame){return thisModule['generate expression []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["target"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:225)');},
							function(frame){return require('./writer')['write []'](ncore.template([", '",ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])),"',["]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:226)');},
							function(frame){return ncore.forEach("Arg", ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args"])), function(frame){return ncore.doAll([
								function(frame){return ncore.doIf([
									[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
										function(frame){return require('./writer')['write []'](",", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:229)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:228)');}],
									], frame);},
								function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:230)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:227)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:227)');},
							function(frame){return require('./writer')['write []']("], ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:231)');},
							function(frame){return ncore.doWhen([
								[require('nnatural/acore')['[] gt []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["children", "length"])), 0, frame), function(frame){return ncore.doAll([
									function(frame){return thisModule['generate action call events frame []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["children"])), frame, 'generate action call events frame [](node_modules/nnatural-compiler/compile-nnt:233)');}], frame, '*when [] gt [](node_modules/nnatural-compiler/compile-nnt:232)');}],
								[true, function(frame){return ncore.doAll([
									function(frame){return require('./writer')['write []']("frame", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:235)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:234)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:234');},
							function(frame){return require('./writer')['write []'](ncore.template([", '",thisModule['[] location in file []'](require('nnatural/acore')['[] head'](i('List', frame), frame), ncore.contextRef('file-name', frame), frame),"');}"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:236)');}], frame, '*when [] is send-message(node_modules/nnatural-compiler/compile-nnt:223)');}],
						[true, function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []']("function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:238)');},
							function(frame){return thisModule['generate resolved name [] namespace []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "name"])), ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:239)');},
							function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:240)');},
							function(frame){return ncore.forEach("Argument", ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["statement", "args"])), function(frame){return ncore.doAll([
								function(frame){return thisModule['generate expression []'](i('Argument', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:242)');},
								function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:243)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:241)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:241)');},
							function(frame){return ncore.doWhen([
								[require('nnatural/acore')['[] gt []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["children", "length"])), 0, frame), function(frame){return ncore.doAll([
									function(frame){return thisModule['generate action call events frame []'](ncore.PathRef(require('nnatural/acore')['[] head'](i('List', frame), frame), v(["children"])), frame, 'generate action call events frame [](node_modules/nnatural-compiler/compile-nnt:245)');}], frame, '*when [] gt [](node_modules/nnatural-compiler/compile-nnt:244)');}],
								[true, function(frame){return ncore.doAll([
									function(frame){return require('./writer')['write []']("frame", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:247)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:246)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:246');},
							function(frame){return require('./writer')['write []'](ncore.template([", '",thisModule['[] location in file []'](require('nnatural/acore')['[] head'](i('List', frame), frame), ncore.contextRef('file-name', frame), frame),"');}"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:248)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:237)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:237');},
					function(frame){return ncore.doWhen([
						[require('nnatural/acore')['[] tail'](i('List', frame), frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:250)');},
							function(frame){return thisModule['generate action list []'](require('nnatural/acore')['[] tail'](i('List', frame), frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:251)');}], frame, '*when [] tail(node_modules/nnatural-compiler/compile-nnt:249)');}],
						[true, function(frame){return ncore.doAll([
							function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:253)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:252)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:252');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:192)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:191)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:191');}], frame, '*generate action list [](node_modules/nnatural-compiler/compile-nnt:153)');},
	'defines of []' : 	function(Entity, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Entity ],function(){ return ncore.PathRef(thisModule['children of [] named []'](Entity, "defines", frame), v(["0", "children"]));});},
	'should wrap []' : 	function(Definition, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Definition ],function(){ return v(ncore.PathRef(Definition, v(["statement", "namespace"])))? true : false;});},
	'argument [] is a reference' : 	function(Arg, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Arg ],function(){ return require('nnatural/acore')['[] eq []'](ncore.PathRef(Arg, v(["name"])), "_reference", frame);});},
	'generate expression definition []' : function(Definition, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Definition = Definition;
	frame['the context-type'] = "expression";
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame['the Role'] = "expression";
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:272)');},
		function(frame){return ncore.doIf([
			[require('nnatural/acore')['[] gt []'](ncore.PathRef(i('Definition', frame), v(["statement", "args", "length"])), 0, frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("function(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:274)');},
				function(frame){return ncore.forEach("Arg", ncore.PathRef(i('Definition', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:277)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:276)');}],
						], frame);},
					function(frame){return require('./writer')['write []'](thisModule['argument [] name'](i('Arg', frame), frame), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:278)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:275)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:275)');},
				function(frame){return require('./writer')['writeln []'](", parent){", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:279)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:280)');},
				function(frame){return require('./writer')['writeln []']("var frame = ncore.Frame(parent,null,parent,'expression');", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:281)');},
				function(frame){return ncore.forEach("Arg", ncore.PathRef(i('Definition', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[thisModule['argument [] is a reference'](i('Arg', frame), frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['writeln []'](ncore.template(["e(frame,",thisModule['argument [] name'](i('Arg', frame), frame),",'",thisModule['[] trimmed'](thisModule['argument [] name'](i('Arg', frame), frame), frame),"');"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:284)');}], frame, '*if argument [] is a reference(node_modules/nnatural-compiler/compile-nnt:283)');}],
						], frame);}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:282)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:282)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:273)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []']("function(frame){", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:286)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:287)');}], frame, '*else(node_modules/nnatural-compiler/compile-nnt:285)');}]], frame);},
		function(frame){return require('./writer')['write []']("return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:288)');},
		function(frame){return ncore.doWhen([
			[thisModule['should wrap []'](i('Definition', frame), frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("ncore.wrap(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:290)');}], frame, '*when should wrap [](node_modules/nnatural-compiler/compile-nnt:289)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:292)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:291)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:291');},
		function(frame){return require('./writer')['write []'](ncore.template(["ncore.waitValue([ ",thisModule['arguments [] list'](ncore.PathRef(i('Definition', frame), v(["statement", "args"])), frame)," ],function(){ return "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:293)');},
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] or []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Definition', frame), v(["children", "length"])), 0, frame), require('nnatural/acore')['[] and []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Definition', frame), v(["children", "length"])), 1, frame), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Definition', frame), v(["children", "0", "statement", "name"])), "meta", frame), frame), frame), function(frame){return ncore.doAll([
				function(frame){return ncore.doWhen([
					[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Definition', frame), v(["statement", "args", "length"])), 0, frame), function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []'](ncore.template(["\"",require('nnatural/acore')['[] after []'](ncore.PathRef(i('Definition', frame), v(["statement", "name"])), "define expression ", frame),"\""]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:298)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:296)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('./writer')['writeln []']("{", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:301)');},
						function(frame){return ncore.forEach("Arg", ncore.PathRef(i('Definition', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
							function(frame){return ncore.doIf([
								[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
									function(frame){return require('./writer')['writeln []'](", ", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:304)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:303)');}],
								], frame);},
							function(frame){return require('./writer')['write []'](ncore.template([ncore.PathRef(i('Arg', frame), v(["args", "0"]))," : v(",ncore.PathRef(i('Arg', frame), v(["args", "0"])),")"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:305)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:302)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:302)');},
						function(frame){return require('./writer')['write []']("}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:306)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:299)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:299');}], frame, '*when [] or [](node_modules/nnatural-compiler/compile-nnt:294)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Definition', frame), v(["children", "0", "statement", "name"])), "meta", frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate expression block []'](require('nnatural/acore')['[] tail'](ncore.PathRef(i('Definition', frame), v(["children"])), frame), frame, 'generate expression block [](node_modules/nnatural-compiler/compile-nnt:308)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:307)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return thisModule['generate expression block []'](ncore.PathRef(i('Definition', frame), v(["children"])), frame, 'generate expression block [](node_modules/nnatural-compiler/compile-nnt:310)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:309)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:309');},
		function(frame){return require('./writer')['write []'](";})", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:311)');},
		function(frame){return ncore.doWhen([
			[thisModule['should wrap []'](i('Definition', frame), frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template([", {$type:'",require('nnatural/acore')['[] default []'](ncore.PathRef(i('Definition', frame), v(["statement", "namespace"])), "expression", frame),"'"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:313)');},
				function(frame){return ncore.doIf([
					[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Definition', frame), v(["statement", "args", "length"])), 0, frame), function(frame){return ncore.doAll([
						function(frame){return require('./writer')['write []'](ncore.template([",$uri:'",require('nnatural/acore')['[] default []'](ncore.contextRef('UriProlog', frame), "nnatural:", frame),thisModule['[] as uri encoded'](ncore.contextRef('file-name', frame), frame),"/",thisModule['[] as uri encoded'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Definition', frame), v(["statement", "name"])), "define expression ", frame), frame),"'"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:315)');}], frame, '*if [] eq [](node_modules/nnatural-compiler/compile-nnt:314)');}],
					], frame);},
				function(frame){return require('./writer')['write []']("});}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:316)');}], frame, '*when should wrap [](node_modules/nnatural-compiler/compile-nnt:312)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:318)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:317)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:317');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:319)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:320)');},
		function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', [require('nnatural/acore')['[] default []'](ncore.PathRef(i('Definition', frame), v(["statement", "namespace"])), "expression", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Definition', frame), v(["statement", "name"])), "define expression ", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Definition', frame), v(["statement", "htmlName"])), "define expression ", frame), ncore.contextRef('stdout', frame), i('Definition', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/compile-nnt:321)');}], frame, '*generate expression definition [](node_modules/nnatural-compiler/compile-nnt:267)');},
	'generate action definition []' : function(Action, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Action = Action;
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return require('./writer')['write []']("function(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:327)');},
			function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Action', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template([thisModule['argument [] name'](i('Argument', frame), frame),", "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:329)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:328)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:328)');},
			function(frame){return require('./writer')['writeln []']("parent, loc){", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:330)');},
			function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:331)');},
			function(frame){return require('./writer')['writeln []']("var frame = ncore.Frame(parent,null,local,'action',loc);", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:332)');},
			function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Action', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []'](ncore.template(["frame.",thisModule['argument [] name'](i('Argument', frame), frame)," = ",thisModule['argument [] name'](i('Argument', frame), frame),";"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:334)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:333)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:333)');},
			function(frame){return ncore.forEach("Define", thisModule['defines of []'](i('Action', frame), frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate define []'](i('Define', frame), frame, 'generate define [](node_modules/nnatural-compiler/compile-nnt:336)');}], frame, '*for each [] in defines of [](node_modules/nnatural-compiler/compile-nnt:335)');}, frame, '*for each [] in defines of [](node_modules/nnatural-compiler/compile-nnt:335)');},
			function(frame){return require('./writer')['writeln []']("frame.this = frame;", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:337)');},
			function(frame){return ncore.forEach("Arg", ncore.PathRef(i('Action', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
				function(frame){return ncore.doIf([
					[thisModule['argument [] is a reference'](i('Arg', frame), frame), function(frame){return ncore.doAll([
						function(frame){return require('./writer')['writeln []'](ncore.template(["e(frame,",thisModule['argument [] name'](i('Arg', frame), frame),",'",thisModule['[] trimmed'](thisModule['argument [] name'](i('Arg', frame), frame), frame),"');"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:342)');}], frame, '*if argument [] is a reference(node_modules/nnatural-compiler/compile-nnt:341)');}],
					], frame);}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:340)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:340)');},
			function(frame){return require('./writer')['write []']("return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:344)');},
			function(frame){return thisModule['generate default dispatcher []'](i('Action', frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:345)');},
			function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:346)');},
			function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:347)');},
			function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:348)');},
			function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', [require('nnatural/acore')['[] default []'](ncore.PathRef(i('Action', frame), v(["statement", "namespace"])), "action", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Action', frame), v(["statement", "name"])), "define action ", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Action', frame), v(["statement", "htmlName"])), "define action ", frame), ncore.contextRef('stdout', frame), i('Action', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/compile-nnt:349)');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:326)');}], frame, '*generate action definition [](node_modules/nnatural-compiler/compile-nnt:323)');},
	'generate jsaction definition []' : function(Action, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Action = Action;
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['write []']("function(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:354)');},
		function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Action', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
			function(frame){return require('./writer')['write []'](ncore.template([thisModule['argument [] name'](i('Argument', frame), frame),", "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:356)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:355)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:355)');},
		function(frame){return require('./writer')['writeln []']("parent, loc){", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:357)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:358)');},
		function(frame){return require('./writer')['writeln []']("var frame = ncore.Frame(parent,null,parent,'jsaction',loc);", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:359)');},
		function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Action', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
			function(frame){return require('./writer')['writeln []'](ncore.template(["frame.",thisModule['argument [] name'](i('Argument', frame), frame)," = ",thisModule['argument [] name'](i('Argument', frame), frame),";"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:361)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:360)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:360)');},
		function(frame){return ncore.forEach("Define", thisModule['defines of []'](i('Action', frame), frame), function(frame){return ncore.doAll([
			function(frame){return thisModule['generate define []'](i('Define', frame), frame, 'generate define [](node_modules/nnatural-compiler/compile-nnt:363)');}], frame, '*for each [] in defines of [](node_modules/nnatural-compiler/compile-nnt:362)');}, frame, '*for each [] in defines of [](node_modules/nnatural-compiler/compile-nnt:362)');},
		function(frame){return require('./writer')['writeln []']("frame._loc = loc;", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:364)');},
		function(frame){return require('./writer')['write []'](ncore.template(["var ret = ncore.waitValue([ ",thisModule['arguments [] list'](ncore.PathRef(i('Action', frame), v(["statement", "args"])), frame)," ],function(){ return "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:365)');},
		function(frame){return require('./writer')['writeln []']("function()", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:366)');},
		function(frame){return require('./writer')['write []'](ncore.PathRef(i('Action', frame), v(["children", "0", "statement", "args", "0", "args", "0"])), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:367)');},
		function(frame){return require('./writer')['writeln []']("();});", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:368)');},
		function(frame){return require('./writer')['writeln []']("if(ncore.isPending(ret)){return ncore.wait(ret,frame);}", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:369)');},
		function(frame){return require('./writer')['writeln []']("if(ret ===true || typeof ret === \"undefined\"){return ncore.complete(frame);}else{return ncore.async(frame);}}", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:370)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:371)');},
		function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', [require('nnatural/acore')['[] default []'](ncore.PathRef(i('Action', frame), v(["statement", "namespace"])), "action", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Action', frame), v(["statement", "name"])), "define jsaction ", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Action', frame), v(["statement", "htmlName"])), "define jsaction ", frame), ncore.contextRef('stdout', frame), i('Action', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/compile-nnt:372)');}], frame, '*generate jsaction definition [](node_modules/nnatural-compiler/compile-nnt:351)');},
	'[] trimmed' : 	function(Text, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Text ],function(){ return v(Text).trim();});},
	'[] quote escaped' : 	function(Text, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Text ],function(){ return v(Text).replace("'","\\'");});},
	'[] location in file []' : 	function(Entity, File, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Entity, File ],function(){ return require('nnatural/acore')['[] starts with []'](ncore.PathRef(Entity, v(["statement", "name"])), "define action ", frame)? thisModule['[] quote escaped'](ncore.template([require('nnatural/acore')['[] after []'](ncore.PathRef(Entity, v(["statement", "name"])), "define action ", frame),"(",File,":",ncore.PathRef(Entity, v(["location", "start", "line"])),")"]), frame) : thisModule['[] quote escaped'](ncore.template([ncore.PathRef(Entity, v(["statement", "name"])),"(",File,":",ncore.PathRef(Entity, v(["location", "start", "line"])),")"]), frame);});},
	'[] arguments' : 	function(Entity, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Entity ],function(){ return v(ncore.PathRef(Entity, v(["args"])))? ncore.PathRef(Entity, v(["args"])) : v(ncore.PathRef(Entity, v(["statement", "args"])))? ncore.PathRef(Entity, v(["statement", "args"])) : null;});},
	'generate when expression []' : function(Block, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Block = Block;
	frame['State'] = require('nnatural/acore')['json'](frame);
	frame['the Statement'] = require('nnatural/acore')['json'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.send(i('State', frame), '[]',["start"], frame, '[](node_modules/nnatural-compiler/compile-nnt:396)');},
		function(frame){return ncore.forEach("Expression", i('Block', frame), function(frame){return ncore.doAll([
			function(frame){return ncore.send(ncore.contextRef('Statement', frame), '[]',[ncore.PathRef(i('Expression', frame), v(["statement"]))], frame, '[](node_modules/nnatural-compiler/compile-nnt:398)');},
			function(frame){return ncore.doWhen([
				[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), "when []", frame), function(frame){return ncore.doAll([
					function(frame){return ncore.send(i('State', frame), '[]',["when"], frame, '[](node_modules/nnatural-compiler/compile-nnt:400)');},
					function(frame){return require('./writer')['write []']("v(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:401)');},
					function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Expression', frame), v(["statement", "args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:402)');},
					function(frame){return require('./writer')['write []'](")? ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:403)');},
					function(frame){return thisModule['generate expression block []'](ncore.PathRef(i('Expression', frame), v(["children"])), frame, 'generate expression block [](node_modules/nnatural-compiler/compile-nnt:404)');},
					function(frame){return require('./writer')['write []'](" : ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:405)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:399)');}],
				[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), "when ", frame), function(frame){return ncore.doAll([
					function(frame){return ncore.send(i('State', frame), '[]',["when"], frame, '[](node_modules/nnatural-compiler/compile-nnt:407)');},
					function(frame){return thisModule['generate resolved name [] namespace []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), "when ", frame), ncore.PathRef(i('Expression', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:408)');},
					function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:409)');},
					function(frame){return ncore.forEach("Element", ncore.PathRef(i('Expression', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
						function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:411)');},
						function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:412)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:410)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:410)');},
					function(frame){return require('./writer')['write []']("frame)? ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:413)');},
					function(frame){return thisModule['generate expression block []'](ncore.PathRef(i('Expression', frame), v(["children"])), frame, 'generate expression block [](node_modules/nnatural-compiler/compile-nnt:414)');},
					function(frame){return require('./writer')['write []'](" : ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:415)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:406)');}],
				[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), "otherwise", frame), function(frame){return ncore.doAll([
					function(frame){return ncore.send(i('State', frame), '[]',["end"], frame, '[](node_modules/nnatural-compiler/compile-nnt:417)');},
					function(frame){return thisModule['generate expression block []'](ncore.PathRef(i('Expression', frame), v(["children"])), frame, 'generate expression block [](node_modules/nnatural-compiler/compile-nnt:418)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:416)');}],
				], frame,'@node_modules/nnatural-compiler/compile-nnt:');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:397)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:397)');},
		function(frame){return ncore.doIf([
			[require('nnatural/acore')['[] neq []'](i('State', frame), "end", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("null", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:420)');}], frame, '*if [] neq [](node_modules/nnatural-compiler/compile-nnt:419)');}],
			], frame);}], frame, '*generate when expression [](node_modules/nnatural-compiler/compile-nnt:392)');},
	'generate expression block []' : function(Block, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Block = Block;
	frame['the Role'] = "expression";
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Block', frame), v(["0", "statement", "name"])), "when ", frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate when expression []'](i('Block', frame), frame, 'generate when expression [](node_modules/nnatural-compiler/compile-nnt:426)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:425)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Block', frame), v(["0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:428)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:427)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:427');}], frame, '*generate expression block [](node_modules/nnatural-compiler/compile-nnt:422)');},
	'generate expression []' : function(Expression, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Expression = Expression;
	frame['the Statement'] = i('Expression', frame);
	frame['the Role'] = "expression";
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] is array'](i('Expression', frame), frame), function(frame){return ncore.doAll([
				function(frame){return ncore.forEach("Element", i('Expression', frame), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:437)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:436)');}],
						], frame);},
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:438)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:435)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:435)');}], frame, '*when [] is array(node_modules/nnatural-compiler/compile-nnt:434)');}],
			[require('nnatural/acore')['[] and []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_variable", frame), require('nnatural/acore')['[] eq []'](ncore.contextRef('context-type', frame), "expression", frame), frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template([ncore.PathRef(i('Expression', frame), v(["args", "0"]))]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:440)');}], frame, '*when [] and [](node_modules/nnatural-compiler/compile-nnt:439)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_variable", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["i('",ncore.PathRef(i('Expression', frame), v(["args", "0"])),"', frame)"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:442)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:441)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_string", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["\"",ncore.PathRef(i('Expression', frame), v(["args", "0"])),"\""]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:444)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:443)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_js", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template([thisModule['[] trimmed'](require('nnatural/acore')['[] from [] to [] before end'](ncore.PathRef(i('Expression', frame), v(["args", "0"])), 1, 1, frame), frame)]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:446)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:445)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_reference", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["r('",ncore.PathRef(i('Expression', frame), v(["ref"])),"','",ncore.PathRef(i('Expression', frame), v(["ns"])),"',frame)"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:448)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:447)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_immediate", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["i('",ncore.PathRef(i('Expression', frame), v(["args", "0"])),"',frame)"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:450)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:449)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_number", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template([ncore.PathRef(i('Expression', frame), v(["args", "0"]))]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:452)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:451)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_context", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["ncore.contextRef('",ncore.PathRef(i('Expression', frame), v(["args", "0"])),"', frame)"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:454)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:453)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_list", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("[", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:456)');},
				function(frame){return ncore.forEach("Element", ncore.PathRef(i('Expression', frame), v(["args"])), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:459)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:458)');}],
						], frame);},
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:460)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:457)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:457)');},
				function(frame){return require('./writer')['write []']("]", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:461)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:455)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_path", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("ncore.PathRef(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:463)');},
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Expression', frame), v(["args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:464)');},
				function(frame){return require('./writer')['write []'](", v([", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:465)');},
				function(frame){return ncore.forEach("Element", ncore.PathRef(i('Expression', frame), v(["args", "1"])), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:468)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:467)');}],
						], frame);},
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:469)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:466)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:466)');},
				function(frame){return require('./writer')['write []']("]))", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:470)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:462)');}],
			[require('nnatural/acore')['[] and []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_defined", frame), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["dispatch"])), "defer", frame), frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []']("function(frame){", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:472)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:473)');},
				function(frame){return require('./writer')['write []']("return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:474)');},
				function(frame){return thisModule['generate resolved name [] namespace []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), ncore.PathRef(i('Expression', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:475)');},
				function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:476)');},
				function(frame){return ncore.forEach("Element", ncore.PathRef(i('Expression', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:478)');},
					function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:479)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:477)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:477)');},
				function(frame){return require('./writer')['write []']("frame)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:480)');},
				function(frame){return require('./writer')['writeln []'](";", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:481)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:482)');},
				function(frame){return require('./writer')['write []']("}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:483)');}], frame, '*when [] and [](node_modules/nnatural-compiler/compile-nnt:471)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_defined", frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate resolved name [] namespace []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), ncore.PathRef(i('Expression', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:485)');},
				function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:486)');},
				function(frame){return ncore.forEach("Element", ncore.PathRef(i('Expression', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:488)');},
					function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:489)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:487)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:487)');},
				function(frame){return require('./writer')['write []']("frame)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:490)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:484)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_template", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("ncore.template([", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:492)');},
				function(frame){return ncore.forEach("Element", thisModule['[] arguments'](i('Expression', frame), frame), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](",", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:495)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:494)');}],
						], frame);},
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:496)');}], frame, '*for each [] in [] arguments(node_modules/nnatural-compiler/compile-nnt:493)');}, frame, '*for each [] in [] arguments(node_modules/nnatural-compiler/compile-nnt:493)');},
				function(frame){return require('./writer')['write []']("])", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:497)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:491)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_true", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("true", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:499)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:498)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_false", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("false", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:501)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:500)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "_null", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []']("null", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:503)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:502)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["name"])), "[]", frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Expression', frame), v(["args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:505)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:504)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), "for each [] in []", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["ncore.forEachExpression('",ncore.PathRef(i('Expression', frame), v(["statement", "args", "0", "args", "0"])),"',"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:507)');},
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Expression', frame), v(["statement", "args", "1"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:508)');},
				function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:509)');},
				function(frame){return require('./writer')['writeln []'](ncore.template(["function(",ncore.PathRef(i('Expression', frame), v(["statement", "args", "0", "args", "0"])),",frame){"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:510)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:511)');},
				function(frame){return require('./writer')['write []']("return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:512)');},
				function(frame){return thisModule['generate expression block []'](ncore.PathRef(i('Expression', frame), v(["children"])), frame, 'generate expression block [](node_modules/nnatural-compiler/compile-nnt:513)');},
				function(frame){return require('./writer')['write []'](";}, frame)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:514)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:515)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:506)');}],
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), "for each [] in ", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["ncore.forEachExpression('",ncore.PathRef(i('Expression', frame), v(["statement", "args", "0", "args", "0"])),"', "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:517)');},
				function(frame){return thisModule['generate resolved name [] namespace []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Expression', frame), v(["statement", "name"])), "for each [] in ", frame), ncore.PathRef(i('Expression', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:518)');},
				function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:519)');},
				function(frame){return ncore.forEach("Element", require('nnatural/acore')['[] tail'](ncore.PathRef(i('Expression', frame), v(["statement", "args"])), frame), function(frame){return ncore.doAll([
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:521)');},
					function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:522)');}], frame, '*for each [] in [] tail(node_modules/nnatural-compiler/compile-nnt:520)');}, frame, '*for each [] in [] tail(node_modules/nnatural-compiler/compile-nnt:520)');},
				function(frame){return require('./writer')['writeln []'](ncore.template(["frame),function(",ncore.PathRef(i('Expression', frame), v(["statement", "args", "0", "args", "0"])),"){"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:523)');},
				function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:524)');},
				function(frame){return require('./writer')['write []']("return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:525)');},
				function(frame){return thisModule['generate expression block []'](ncore.PathRef(i('Expression', frame), v(["children"])), frame, 'generate expression block [](node_modules/nnatural-compiler/compile-nnt:526)');},
				function(frame){return require('./writer')['write []'](";},frame)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:527)');},
				function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:528)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:516)');}],
			[ncore.PathRef(i('Expression', frame), v(["name"])),function(frame){return ncore.doAll([
				function(frame){return thisModule['generate resolved name [] namespace []'](ncore.PathRef(i('Expression', frame), v(["name"])), ncore.PathRef(i('Expression', frame), v(["namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:530)');},
				function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:531)');},
				function(frame){return ncore.forEach("Element", ncore.PathRef(i('Expression', frame), v(["args"])), function(frame){return ncore.doAll([
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:533)');},
					function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:534)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:532)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:532)');},
				function(frame){return require('./writer')['write []']("frame)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:535)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:529)');}],
			[require('nnatural/acore')['[] and []'](ncore.PathRef(i('Expression', frame), v(["statement"])), require('nnatural/acore')['not []'](ncore.PathRef(i('Expression', frame), v(["name"])), frame), frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Expression', frame), v(["statement"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:537)');}], frame, '*when [] and [](node_modules/nnatural-compiler/compile-nnt:536)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Expression', frame), v(["_type"])), "free-text", frame), function(frame){return ncore.doAll([
				function(frame){return ncore.report(frame, 'parse error at []', [ncore.PathRef(i('Expression', frame), v(["location"]))], 'parse error at [](node_modules/nnatural-compiler/compile-nnt:539)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:538)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['console log error []'](["compiling failure - failed to generate the expression ", i('Expression', frame)], frame, 'console log error [](node_modules/nnatural-compiler/compile-nnt:542)');},
				function(frame){return ncore.report(frame, 'error []', ["Error compiling file - details in log"], 'error [](node_modules/nnatural-compiler/compile-nnt:543)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:540)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:540');}], frame, '*generate expression [](node_modules/nnatural-compiler/compile-nnt:430)');},
	'[] is send-message' : 	function(Action, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Action ],function(){ return ;});},
	'[] is send-report' : 	function(Action, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Action ],function(){ return require('nnatural/acore')['[] and []'](ncore.PathRef(Action, v(["target"])), require('nnatural/acore')['[] eq []'](ncore.PathRef(Action, v(["target", "name"])), "_report", frame), frame)? true : false;});},
	'[] is action' : 	function(Child, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Child ],function(){ return require('nnatural/acore')['[] eq []'](ncore.PathRef(Child, v(["statement", "name"])), "defines", frame)? false : true;});},
	'generate handler []' : function(Handler, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Handler = Handler;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['write []']("function(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:564)');},
		function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Handler', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
			function(frame){return require('./writer')['write []'](ncore.template([thisModule['argument [] name'](i('Argument', frame), frame),", "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:566)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:565)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:565)');},
		function(frame){return require('./writer')['writeln []']("parent, reportTo, origin, loc){", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:567)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:568)');},
		function(frame){return require('./writer')['writeln []']("var frame = ncore.Frame(reportTo, null, parent,'message-handler',loc);", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:569)');},
		function(frame){return require('./writer')['writeln []']("frame._origin = origin;", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:570)');},
		function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Handler', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
			function(frame){return require('./writer')['writeln []'](ncore.template(["frame.",thisModule['argument [] name'](i('Argument', frame), frame)," = ",thisModule['argument [] name'](i('Argument', frame), frame),";"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:572)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:571)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:571)');},
		function(frame){return ncore.forEach("Define", ncore.PathRef(i('Handler', frame), v(["children"])), function(frame){return ncore.doAll([
			function(frame){return thisModule['generate define []'](i('Define', frame), frame, 'generate define [](node_modules/nnatural-compiler/compile-nnt:574)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:573)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:573)');},
		function(frame){return require('./writer')['write []']("return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:575)');},
		function(frame){return thisModule['generate default dispatcher []'](i('Handler', frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:576)');},
		function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:577)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:578)');}], frame, '*generate handler [](node_modules/nnatural-compiler/compile-nnt:563)');},
	'[] trimmed' : 	function(Text, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Text ],function(){ return v(Text).trim();});},
	'generate define []' : function(Define, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Define = Define;
	frame['the Statement'] = ncore.PathRef(i('Define', frame), v(["statement"]));
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), "on ", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["frame._events['",require('nnatural/acore')['[] after []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), "on ", frame),"'] = "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:587)');},
				function(frame){return thisModule['generate handler []'](i('Define', frame), frame, 'generate handler [](node_modules/nnatural-compiler/compile-nnt:588)');},
				function(frame){return require('./writer')['writeln []'](";", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:589)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:586)');}],
			[require('nnatural/acore')['[] and []'](ncore.PathRef(i('Define', frame), v(["key"])), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), "[]", frame), frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["frame['",thisModule['[] trimmed'](ncore.PathRef(i('Define', frame), v(["key", "text"])), frame),"'] = "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:591)');},
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Define', frame), v(["statement", "args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:592)');},
				function(frame){return require('./writer')['writeln []'](";", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:593)');}], frame, '*when [] and [](node_modules/nnatural-compiler/compile-nnt:590)');}],
			[ncore.PathRef(i('Define', frame), v(["key"])),function(frame){return ncore.doAll([
				function(frame){return require('./writer')['write []'](ncore.template(["frame['",thisModule['[] trimmed'](ncore.PathRef(i('Define', frame), v(["key", "text"])), frame),"'] = "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:595)');},
				function(frame){return thisModule['generate resolved name [] namespace []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), ncore.PathRef(i('Define', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:596)');},
				function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:597)');},
				function(frame){return ncore.forEach("Element", thisModule['[] arguments'](i('Define', frame), frame), function(frame){return ncore.doAll([
					function(frame){return thisModule['generate expression []'](i('Element', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:599)');},
					function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:600)');}], frame, '*for each [] in [] arguments(node_modules/nnatural-compiler/compile-nnt:598)');}, frame, '*for each [] in [] arguments(node_modules/nnatural-compiler/compile-nnt:598)');},
				function(frame){return require('./writer')['writeln []']("frame);", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:601)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:594)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:603)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:602)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:602');}], frame, '*generate define [](node_modules/nnatural-compiler/compile-nnt:583)');},
	'generate argument []' : function(Argument, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Argument = Argument;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['write []'](ncore.PathRef(i('Argument', frame), v(["args", "0"])), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:606)');}], frame, '*generate argument [](node_modules/nnatural-compiler/compile-nnt:605)');},
	'generate do all dispatcher []' : function(Dispatcher, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Dispatcher = Dispatcher;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return require('./writer')['writeln []']("ncore.doAll([", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:610)');},
			function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:611)');},
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Dispatcher', frame), v(["children", "length"])), 0, frame), function(frame){return ncore.doAll([
					function(frame){return thisModule['warn []'](ncore.template(["got empty action list at ",ncore.contextRef('file-name', frame),":",ncore.PathRef(i('Dispatcher', frame), v(["location", "start", "line"]))]), frame, 'warn [](node_modules/nnatural-compiler/compile-nnt:613)');}], frame, '*if [] eq [](node_modules/nnatural-compiler/compile-nnt:612)');}],
				], frame);},
			function(frame){return thisModule['generate action list []'](ncore.PathRef(i('Dispatcher', frame), v(["children"])), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:614)');},
			function(frame){return require('./writer')['write []']("], frame, '", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:615)');},
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['not []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "do all", frame), frame), function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []']("*", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:617)');}], frame, '*if not [](node_modules/nnatural-compiler/compile-nnt:616)');}],
				], frame);},
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "define action ", frame),function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "define action ", frame), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:619)');}], frame, '*if [](node_modules/nnatural-compiler/compile-nnt:618)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:621)');}], frame, '*else(node_modules/nnatural-compiler/compile-nnt:620)');}]], frame);},
			function(frame){return require('./writer')['write []'](ncore.template(["(",ncore.contextRef('file-name', frame),":",ncore.PathRef(i('Dispatcher', frame), v(["location", "start", "line"])),")')"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:622)');},
			function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:623)');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:609)');}], frame, '*generate do all dispatcher [](node_modules/nnatural-compiler/compile-nnt:608)');},
	'generate default dispatcher []' : function(Dispatcher, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Dispatcher = Dispatcher;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return require('./writer')['writeln []'](ncore.template([require('nnatural/acore')['[] default []'](ncore.contextRef('DefaultDispatcher', frame), "ncore.doAll", frame),"(["]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:627)');},
			function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:628)');},
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Dispatcher', frame), v(["children", "length"])), 0, frame), function(frame){return ncore.doAll([
					function(frame){return thisModule['warn []'](ncore.template(["got empty action list at ",ncore.contextRef('file-name', frame),":",ncore.PathRef(i('Dispatcher', frame), v(["location", "start", "line"]))]), frame, 'warn [](node_modules/nnatural-compiler/compile-nnt:630)');}], frame, '*if [] eq [](node_modules/nnatural-compiler/compile-nnt:629)');}],
				], frame);},
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Dispatcher', frame), v(["children", "0", "statement", "text"])), "meta", frame), function(frame){return ncore.doAll([
					function(frame){return thisModule['generate action list []'](require('nnatural/acore')['[] tail'](ncore.PathRef(i('Dispatcher', frame), v(["children"])), frame), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:632)');}], frame, '*if [] eq [](node_modules/nnatural-compiler/compile-nnt:631)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return thisModule['generate action list []'](ncore.PathRef(i('Dispatcher', frame), v(["children"])), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:634)');}], frame, '*else(node_modules/nnatural-compiler/compile-nnt:633)');}]], frame);},
			function(frame){return require('./writer')['write []']("], frame, '", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:635)');},
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['not []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "do all", frame), frame), function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []']("*", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:637)');}], frame, '*if not [](node_modules/nnatural-compiler/compile-nnt:636)');}],
				], frame);},
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "define action ", frame),function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "define action ", frame), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:639)');}], frame, '*if [](node_modules/nnatural-compiler/compile-nnt:638)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:641)');}], frame, '*else(node_modules/nnatural-compiler/compile-nnt:640)');}]], frame);},
			function(frame){return require('./writer')['write []'](ncore.template(["(",ncore.contextRef('file-name', frame),":",ncore.PathRef(i('Dispatcher', frame), v(["location", "start", "line"])),")')"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:642)');},
			function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:643)');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:626)');}], frame, '*generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:625)');},
	'generate do in order dispatcher []' : function(Dispatcher, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Dispatcher = Dispatcher;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return require('./writer')['writeln []']("ncore.doInOrder([", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:647)');},
			function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:648)');},
			function(frame){return thisModule['generate action list []'](ncore.PathRef(i('Dispatcher', frame), v(["children"])), frame, 'generate action list [](node_modules/nnatural-compiler/compile-nnt:649)');},
			function(frame){return require('./writer')['write []']("], frame, '", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:650)');},
			function(frame){return require('./writer')['write []'](ncore.template([ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])),"(",ncore.contextRef('file-name', frame),":",ncore.PathRef(i('Dispatcher', frame), v(["location", "start", "line"])),")')"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:651)');},
			function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:652)');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:646)');}], frame, '*generate do in order dispatcher [](node_modules/nnatural-compiler/compile-nnt:645)');},
	'identifier []' : 	function(X, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ X ],function(){ return require('nnatural/acore')['[] eq []'](ncore.PathRef(X, v(["name"])), "_reference", frame)? JSON.stringify({$isMetaWrapper:true,$type:"type",value:v(X).ref}) : require('nnatural/acore')['[] eq []'](ncore.PathRef(X, v(["name"])), "_variable", frame)? JSON.stringify(v(X).args[0]) : null;});},
	'generate for each dispatcher []' : function(Dispatcher, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Dispatcher = Dispatcher;
	frame['the Statement'] = ncore.PathRef(i('Dispatcher', frame), v(["statement"]));
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['write []'](ncore.template(["ncore.forEach(",require('nnatural/acore')['[] as text'](thisModule['identifier []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "args", "0"])), frame), frame),", "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:662)');},
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] eq []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "for each [] in ", frame), "[]", frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "args", "1"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:664)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:663)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return thisModule['generate resolved name [] namespace []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "for each [] in ", frame), ncore.PathRef(i('Dispatcher', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:666)');},
				function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:667)');},
				function(frame){return ncore.forEach("Arg", require('nnatural/acore')['[] tail'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "args"])), frame), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:670)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:669)');}],
						], frame);},
					function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:671)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:668)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:668)');},
				function(frame){return require('./writer')['write []'](", frame)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:672)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:665)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:665');},
		function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:673)');},
		function(frame){return require('./writer')['write []']("function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:674)');},
		function(frame){return thisModule['generate default dispatcher []'](i('Dispatcher', frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:675)');},
		function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:676)');},
		function(frame){return require('./writer')['write []'](ncore.template([", frame, '*",thisModule['[] location in file []'](i('Dispatcher', frame), ncore.contextRef('file-name', frame), frame),"')"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:677)');}], frame, '*generate for each dispatcher [](node_modules/nnatural-compiler/compile-nnt:659)');},
	'generate do in order for each dispatcher []' : function(Dispatcher, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Dispatcher = Dispatcher;
	frame['the Statement'] = ncore.PathRef(i('Dispatcher', frame), v(["statement"]));
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['write []'](ncore.template(["ncore.forEachInOrder('",ncore.PathRef(i('Dispatcher', frame), v(["statement", "args", "0", "args", "0"])),"', "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:682)');},
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] eq []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "do in order for each [] in ", frame), "[]", frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "args", "1"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:684)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:683)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return thisModule['generate resolved name [] namespace []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "name"])), "do in order for each [] in ", frame), ncore.PathRef(i('Dispatcher', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:686)');},
				function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:687)');},
				function(frame){return ncore.forEach("Arg", require('nnatural/acore')['[] tail'](ncore.PathRef(i('Dispatcher', frame), v(["statement", "args"])), frame), function(frame){return ncore.doAll([
					function(frame){return ncore.doIf([
						[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
							function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:690)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:689)');}],
						], frame);},
					function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:691)');}], frame, '*for each [] in [] tail(node_modules/nnatural-compiler/compile-nnt:688)');}, frame, '*for each [] in [] tail(node_modules/nnatural-compiler/compile-nnt:688)');},
				function(frame){return require('./writer')['write []'](", frame)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:692)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:685)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:685');},
		function(frame){return require('./writer')['write []'](", ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:693)');},
		function(frame){return require('./writer')['write []']("function(frame){return ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:694)');},
		function(frame){return thisModule['generate default dispatcher []'](i('Dispatcher', frame), frame, 'generate default dispatcher [](node_modules/nnatural-compiler/compile-nnt:695)');},
		function(frame){return require('./writer')['write []'](";}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:696)');},
		function(frame){return require('./writer')['write []'](ncore.template([", frame, '*",thisModule['[] location in file []'](i('Dispatcher', frame), ncore.contextRef('file-name', frame), frame),"')"]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:697)');}], frame, '*generate do in order for each dispatcher [](node_modules/nnatural-compiler/compile-nnt:679)');},
	'generate module declaration []' : function(Module, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Module = Module;
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doIf([
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Module', frame), v(["statement", "name"])), "module []", frame), function(frame){return ncore.doAll([
				function(frame){return ncore.report(frame, 'module []', [ncore.PathRef(i('Module', frame), v(["statement", "args", "0", "args", "0"]))], 'module [](node_modules/nnatural-compiler/compile-nnt:703)');}], frame, '*if [] eq [](node_modules/nnatural-compiler/compile-nnt:702)');}],
			], frame);},
		function(frame){return ncore.forEach("Import", ncore.PathRef(thisModule['child of [] named []'](i('Module', frame), "imports", frame), v(["children"])), function(frame){return ncore.doAll([
			function(frame){return ncore.send(ncore.contextRef('entity-resolver', frame), 'push library []',[ncore.PathRef(i('Import', frame), v(["statement", "args", "0", "args", "0"]))], frame, 'push library [](node_modules/nnatural-compiler/compile-nnt:705)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:704)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:704)');},
		function(frame){return thisModule['generate module local []'](thisModule['child of [] named []'](i('Module', frame), "local", frame), frame, 'generate module local [](node_modules/nnatural-compiler/compile-nnt:706)');}], frame, '*generate module declaration [](node_modules/nnatural-compiler/compile-nnt:699)');},
	'key [] name' : 	function(Key, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Key ],function(){ return require('nnatural/acore')['[] eq []'](ncore.PathRef(Key, v(["name"])), "[]", frame)? thisModule['[] trimmed'](ncore.PathRef(Key, v(["text"])), frame) : ncore.PathRef(Key, v(["name"]));});},
	'generate module local []' : function(Local, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Local = Local;
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['writeln []']("{", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:717)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:718)');},
		function(frame){return require('./writer')['write []']("'isFrame' : true", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:719)');},
		function(frame){return ncore.forEach("Define", ncore.PathRef(i('Local', frame), v(["children"])), function(frame){return ncore.doAll([
			function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:721)');},
			function(frame){return ncore.doWhen([
				[require('nnatural/acore')['[] and []'](ncore.PathRef(i('Define', frame), v(["key"])), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), "[]", frame), frame), function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](ncore.template(["'",thisModule['key [] name'](ncore.PathRef(i('Define', frame), v(["key"])), frame),"' : "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:723)');},
					function(frame){return thisModule['generate expression []'](ncore.PathRef(i('Define', frame), v(["statement", "args", "0"])), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:724)');}], frame, '*when [] and [](node_modules/nnatural-compiler/compile-nnt:722)');}],
				[require('nnatural/acore')['[] and []'](ncore.PathRef(i('Define', frame), v(["key"])), thisModule['[] arguments'](i('Define', frame), frame), frame), function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](ncore.template(["'",thisModule['key [] name'](ncore.PathRef(i('Define', frame), v(["key"])), frame),"' : "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:726)');},
					function(frame){return thisModule['generate resolved name [] namespace []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), ncore.PathRef(i('Define', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:727)');},
					function(frame){return require('./writer')['write []']("(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:728)');},
					function(frame){return ncore.forEach("Arg", ncore.PathRef(i('Define', frame), v(["args"])), function(frame){return ncore.doAll([
						function(frame){return ncore.doIf([
							[require('nnatural/acore')['[] gt []'](i('Pos', frame), 1, frame), function(frame){return ncore.doAll([
								function(frame){return require('./writer')['write []'](",", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:731)');}], frame, '*if [] gt [](node_modules/nnatural-compiler/compile-nnt:730)');}],
							], frame);},
						function(frame){return thisModule['generate expression []'](i('Arg', frame), frame, 'generate expression [](node_modules/nnatural-compiler/compile-nnt:732)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:729)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:729)');},
					function(frame){return require('./writer')['write []'](", null)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:733)');}], frame, '*when [] and [](node_modules/nnatural-compiler/compile-nnt:725)');}],
				[ncore.PathRef(i('Define', frame), v(["key"])),function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](ncore.template(["'",thisModule['key [] name'](ncore.PathRef(i('Define', frame), v(["key"])), frame),"' : "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:735)');},
					function(frame){return thisModule['generate resolved name [] namespace []'](ncore.PathRef(i('Define', frame), v(["statement", "name"])), ncore.PathRef(i('Define', frame), v(["statement", "namespace"])), frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:736)');},
					function(frame){return require('./writer')['write []']("(null)", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:737)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:734)');}],
				], frame,'@node_modules/nnatural-compiler/compile-nnt:');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:720)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:720)');},
		function(frame){return require('./writer')['writeln []']("}", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:738)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:739)');},
		function(frame){return ncore.report(frame, 'module local []', [ncore.contextRef('stdout', frame)], 'module local [](node_modules/nnatural-compiler/compile-nnt:740)');}], frame, '*generate module local [](node_modules/nnatural-compiler/compile-nnt:714)');},
	'[] with [] replacing all []' : 	function(Text, Replacement, Substr, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Text, Replacement, Substr ],function(){ return (v(Text)||"").replace(new RegExp(v(Substr),"g"),Replacement);});},
	'concat []' : 	function(List, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ List ],function(){ return v(List).join("");});},
	'generate rewrite definition []' : function(Entity, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Entity = Entity;
	frame['stdoud'] = require('./writer')['string writer'](frame);
	frame['Target'] = require('nnatural/acore')['json'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('nnatural/acore')['set [] to []'](i('Target', frame), ncore.PathRef(i('Entity', frame), v(["children", "0", "statement", "text"])), frame, 'set [] to [](node_modules/nnatural-compiler/compile-nnt:752)');},
		function(frame){return ncore.doIf([
			[require('nnatural/acore')['not []'](i('Target', frame), frame), function(frame){return ncore.doAll([
				function(frame){return ncore.report(frame, 'compile error [] at []', ["rewrite entity doesn't have a valid replacement statemet", require('nnatural/acore')['[] default []'](ncore.PathRef(i('Entity', frame), v(["children", "0", "location"])), ncore.PathRef(i('Entity', frame), v(["children", "location"])), frame)], 'compile error [] at [](node_modules/nnatural-compiler/compile-nnt:754)');}], frame, '*if not [](node_modules/nnatural-compiler/compile-nnt:753)');}],
			], frame);},
		function(frame){return ncore.forEach("Arg", ncore.PathRef(i('Entity', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
			function(frame){return require('nnatural/acore')['set [] to []'](i('Target', frame), thisModule['[] with [] replacing all []'](i('Target', frame), thisModule['concat []'](["[", i('Pos', frame), "]"], frame), ncore.PathRef(i('Arg', frame), v(["args", "0"])), frame), frame, 'set [] to [](node_modules/nnatural-compiler/compile-nnt:757)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:756)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:756)');},
		function(frame){return ncore.report(frame, 'rewrite [] type [] html name [] to []', [require('nnatural/acore')['[] after []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define rewrite ", frame), ncore.PathRef(i('Entity', frame), v(["statement", "namespace"])), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Entity', frame), v(["statement", "htmlName"])), "define rewrite ", frame), i('Target', frame)], 'rewrite [] type [] html name [] to [](node_modules/nnatural-compiler/compile-nnt:758)');}], frame, '*generate rewrite definition [](node_modules/nnatural-compiler/compile-nnt:748)');},
	'generate component definition []' : function(Component, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Component = Component;
	frame['the stdout'] = require('./writer')['string writer'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['write []']("function(", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:763)');},
		function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Component', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
			function(frame){return require('./writer')['write []'](ncore.template([thisModule['argument [] name'](i('Argument', frame), frame),", "]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:765)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:764)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:764)');},
		function(frame){return require('./writer')['writeln []']("parent){", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:766)');},
		function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:767)');},
		function(frame){return require('./writer')['writeln []']("var frame = ncore.Frame(null,null,local,'component');", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:768)');},
		function(frame){return require('./writer')['writeln []']("frame.this = frame;", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:769)');},
		function(frame){return require('./writer')['writeln []'](ncore.template(["frame.$type = \"",require('nnatural/acore')['[] default []'](ncore.PathRef(i('Component', frame), v(["statement", "namespace"])), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Component', frame), v(["statement", "name"])), "define component ", frame), frame),"\";"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:770)');},
		function(frame){return ncore.forEach("Argument", ncore.PathRef(i('Component', frame), v(["statement", "args"])), function(frame){return ncore.doAll([
			function(frame){return require('./writer')['writeln []'](ncore.template(["frame.",thisModule['argument [] name'](i('Argument', frame), frame)," = ",thisModule['argument [] name'](i('Argument', frame), frame),";"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:772)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:771)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:771)');},
		function(frame){return ncore.forEach("Define", ncore.PathRef(i('Component', frame), v(["children"])), function(frame){return ncore.doAll([
			function(frame){return thisModule['generate define []'](i('Define', frame), frame, 'generate define [](node_modules/nnatural-compiler/compile-nnt:774)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:773)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:773)');},
		function(frame){return require('./writer')['write []']("return frame;}", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:775)');},
		function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:776)');},
		function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', [require('nnatural/acore')['[] default []'](ncore.PathRef(i('Definition', frame), v(["statement", "namespace"])), "component", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Component', frame), v(["statement", "name"])), "define component ", frame), require('nnatural/acore')['[] after []'](ncore.PathRef(i('Definition', frame), v(["statement", "htmlName"])), "define component ", frame), ncore.contextRef('stdout', frame), i('Component', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/compile-nnt:777)');}], frame, '*generate component definition [](node_modules/nnatural-compiler/compile-nnt:760)');},
	'[] without type' : 	function(Name, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Name ],function(){ return v(Name).replace(/\[.*?\]/g,"[]");});},
	'generate module []' : function(Module, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Module = Module;
	frame['First'] = require('nnatural/acore')['json'](frame);
	frame._events['nnt [] named [] html name [] with body [] source []'] = function(Type, Name, HTMLName, Body, Source, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler',loc);
		frame._origin = origin;
		frame.Type = Type;
		frame.Name = Name;
		frame.HTMLName = HTMLName;
		frame.Body = Body;
		frame.Source = Source;
		return ncore.doAll([
			function(frame){return require('./writer')['write []'](ncore.template(["'",thisModule['[] without type'](i('Name', frame), frame),"' : ",i('Body', frame)]), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:786)');},
			function(frame){return ncore.report(frame, 'nnt [] named [] html name [] with body [] source []', [i('Type', frame), i('Name', frame), i('HTMLName', frame), i('Body', frame), i('Source', frame)], 'nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/compile-nnt:787)');}], frame, '*on nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/compile-nnt:785)');};
	frame._events['module local []'] = function(Local, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler',loc);
		frame._origin = origin;
		frame.Local = Local;
		return ncore.doAll([
			function(frame){return require('./writer')['writeln []'](ncore.template(["var local = ",i('Local', frame),";"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:789)');},
			function(frame){return ncore.report(frame, 'module local []', [i('Local', frame)], 'module local [](node_modules/nnatural-compiler/compile-nnt:790)');}], frame, '*on module local [](node_modules/nnatural-compiler/compile-nnt:788)');};
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return ncore.send(i('First', frame), '[]',[true], frame, '[](node_modules/nnatural-compiler/compile-nnt:792)');},
			function(frame){return thisModule['write module prolog for []'](i('Module', frame), frame, 'write module prolog for [](node_modules/nnatural-compiler/compile-nnt:793)');},
			function(frame){return thisModule['generate manifest for []'](i('Module', frame), ncore.Frame(frame, {
				'[]' : function(Manifest, parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler',loc);
					frame._origin = origin;
					frame.Manifest = Manifest;
					return ncore.doAll([
						function(frame){return ncore.send(ncore.contextRef('entity-resolver', frame), 'push manifest [] named []',[i('Manifest', frame),"this"], frame, 'push manifest [] named [](node_modules/nnatural-compiler/compile-nnt:796)');}], frame, '*on [](node_modules/nnatural-compiler/compile-nnt:795)');}},frame,'handlers','*node_modules/nnatural-compiler/compile-nnt:794'), 'generate manifest for [](node_modules/nnatural-compiler/compile-nnt:794)');},
			function(frame){return require('./writer')['writeln []']("var thisModule = {", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:797)');},
			function(frame){return require('./writer')['indent'](frame, 'indent(node_modules/nnatural-compiler/compile-nnt:798)');},
			function(frame){return ncore.forEachInOrder('Entity', ncore.PathRef(i('Module', frame), v(["children"])), function(frame){return ncore.doAll([
				function(frame){return ncore.doWhen([
					[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define action", frame), function(frame){return ncore.doAll([
						function(frame){return ncore.doIf([
							[require('nnatural/acore')['not []'](i('First', frame), frame), function(frame){return ncore.doAll([
								function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:802)');}], frame, '*if not [](node_modules/nnatural-compiler/compile-nnt:801)');}],
							], frame);},
						function(frame){return ncore.send(i('First', frame), '[]',[false], frame, '[](node_modules/nnatural-compiler/compile-nnt:803)');},
						function(frame){return thisModule['generate action definition []'](i('Entity', frame), frame, 'generate action definition [](node_modules/nnatural-compiler/compile-nnt:804)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:800)');}],
					[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define component", frame), function(frame){return ncore.doAll([
						function(frame){return ncore.doIf([
							[require('nnatural/acore')['not []'](i('First', frame), frame), function(frame){return ncore.doAll([
								function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:812)');}], frame, '*if not [](node_modules/nnatural-compiler/compile-nnt:811)');}],
							], frame);},
						function(frame){return ncore.send(i('First', frame), '[]',[false], frame, '[](node_modules/nnatural-compiler/compile-nnt:813)');},
						function(frame){return thisModule['generate component definition []'](i('Entity', frame), frame, 'generate component definition [](node_modules/nnatural-compiler/compile-nnt:814)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:810)');}],
					[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define expression ", frame), function(frame){return ncore.doAll([
						function(frame){return ncore.doIf([
							[require('nnatural/acore')['not []'](i('First', frame), frame), function(frame){return ncore.doAll([
								function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:817)');}], frame, '*if not [](node_modules/nnatural-compiler/compile-nnt:816)');}],
							], frame);},
						function(frame){return ncore.send(i('First', frame), '[]',[false], frame, '[](node_modules/nnatural-compiler/compile-nnt:818)');},
						function(frame){return thisModule['generate expression definition []'](i('Entity', frame), frame, 'generate expression definition [](node_modules/nnatural-compiler/compile-nnt:819)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:815)');}],
					[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define jsaction ", frame), function(frame){return ncore.doAll([
						function(frame){return ncore.doIf([
							[require('nnatural/acore')['not []'](i('First', frame), frame), function(frame){return ncore.doAll([
								function(frame){return require('./writer')['writeln []'](",", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:822)');}], frame, '*if not [](node_modules/nnatural-compiler/compile-nnt:821)');}],
							], frame);},
						function(frame){return ncore.send(i('First', frame), '[]',[false], frame, '[](node_modules/nnatural-compiler/compile-nnt:823)');},
						function(frame){return thisModule['generate jsaction definition []'](i('Entity', frame), frame, 'generate jsaction definition [](node_modules/nnatural-compiler/compile-nnt:824)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:820)');}],
					[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define rewrite ", frame), function(frame){return ncore.doAll([
						function(frame){return thisModule['generate rewrite definition []'](i('Entity', frame), frame, 'generate rewrite definition [](node_modules/nnatural-compiler/compile-nnt:826)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:825)');}],
					[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "use []", frame), function(frame){return ncore.doAll([
						function(frame){return ncore.send(ncore.contextRef('entity-resolver', frame), 'push library []',[ncore.PathRef(i('Entity', frame), v(["statement", "args", "0", "args", "0"]))], frame, 'push library [](node_modules/nnatural-compiler/compile-nnt:828)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:827)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:830)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:829)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:829');}], frame, '*do in order for each [] in [](node_modules/nnatural-compiler/compile-nnt:799)');}, frame, '*do in order for each [] in [](node_modules/nnatural-compiler/compile-nnt:799)');},
			function(frame){return require('./writer')['dedent'](frame, 'dedent(node_modules/nnatural-compiler/compile-nnt:831)');},
			function(frame){return require('./writer')['writeln []']("};", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:832)');},
			function(frame){return require('./writer')['writeln []']("module.exports = thisModule;", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:833)');},
			function(frame){return ncore.doWhen([
				[thisModule['[] has action named []'](i('Module', frame), "main", frame), function(frame){return ncore.doAll([
					function(frame){return require('./writer')['writeln []']("ncore.dispatch(module.exports.main,[ncore.isNEngineContext()? null : ", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:835)');},
					function(frame){return thisModule['generate resolved name [] namespace []']("shell", "nnatural/acore", frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:836)');},
					function(frame){return require('./writer')['write []']("(null),null]);", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:837)');}], frame, '*when [] has action named [](node_modules/nnatural-compiler/compile-nnt:834)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:839)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:838)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:838');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:791)');}], frame, '*generate module [](node_modules/nnatural-compiler/compile-nnt:782)');},
	'generate []' : function(Entity, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Entity = Entity;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define action ", frame), function(frame){return ncore.doAll([
				function(frame){return require('./writer')['writeln []'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), "define action ", frame), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:843)');}], frame, '*when [] starts with [](node_modules/nnatural-compiler/compile-nnt:842)');}],
			], frame,'@node_modules/nnatural-compiler/compile-nnt:');}], frame, '*generate [](node_modules/nnatural-compiler/compile-nnt:841)');},
	'[] has action named []' : 	function(Module, Name, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Module, Name ],function(){ return thisModule['[] has child named []'](Module, ncore.template(["define action ",Name]), frame);});},
	'children of [] named []' : 	function(Block, Name, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Block, Name ],function(){ return ncore.forEachExpression('Child',ncore.PathRef(Block, v(["children"])), function(Child,frame){
			return require('nnatural/acore')['[] eq []'](ncore.PathRef(Child, v(["statement", "name"])), Name, frame)? Child : null;}, frame);});},
	'child of [] named []' : 	function(Block, Name, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Block, Name ],function(){ return require('nnatural/acore')['[] head'](thisModule['children of [] named []'](Block, Name, frame), frame);});},
	'[] has child named []' : 	function(Block, Name, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ Block, Name ],function(){ return require('nnatural/acore')['[] gt []'](require('nnatural/acore')['length of []'](thisModule['children of [] named []'](Block, Name, frame), frame), 0, frame);});},
	'parse nnt source []' : function(Source, parent, loc){
	var frame = ncore.Frame(parent,null,parent,'jsaction',loc);
	frame.Source = Source;
	frame._loc = loc;
	var ret = ncore.waitValue([ Source ],function(){ return function()
	{
		var parsed = "";
		try{
			//rstart rule is a more relaxed rule where statement can include all characters
			parsed = require("./nnt-parser").parse(v(Source),{startRule:'rstart'});
			ncore.asyncReport(frame, "[]",[parsed]);
			return true;
		}catch(e){
			console.log(JSON.stringify(e));
			ncore.asyncReport(frame,"error [] parsing file [] at []",[e.toString(),ncore.contextRef("file-name"),e.location]);
			return true;
		}
		return true;
	}();});
	if(ncore.isPending(ret)){return ncore.wait(ret,frame);}
	if(ret ===true || typeof ret === "undefined"){return ncore.complete(frame);}else{return ncore.async(frame);}}
,
	'[] or [] or []' : 	function(X, Y, Z, parent){
		var frame = ncore.Frame(parent,null,parent,'expression');
		return ncore.waitValue([ X, Y, Z ],function(){ return require('nnatural/acore')['[] or []'](require('nnatural/acore')['[] or []'](X, Y, frame), Z, frame);});},
	'validate []' : function(Source, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Source = Source;
	frame['parsed'] = require('nnatural/acore')['json'](frame);
	frame._events['error []'] = function(Error, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler',loc);
		frame._origin = origin;
		frame.Error = Error;
		return ncore.doAll([
			function(frame){return ncore.report(frame, 'error []', [i('Error', frame)], 'error [](node_modules/nnatural-compiler/compile-nnt:880)');},
			function(frame){return ncore.exitDoInOrder(frame, 'exit do in order(node_modules/nnatural-compiler/compile-nnt:881)');}], frame, '*on error [](node_modules/nnatural-compiler/compile-nnt:879)');};
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doInOrder([
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['[] or []'](require('nnatural/acore')['not []'](require('nnatural/acore')['[] is string'](i('Source', frame), frame), frame), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Source', frame), v(["length"])), 0, frame), frame), function(frame){return ncore.doAll([
					function(frame){return ncore.report(frame, 'error []', ["Error - trying to parse empty string"], 'error [](node_modules/nnatural-compiler/compile-nnt:884)');}], frame, '*if [] or [](node_modules/nnatural-compiler/compile-nnt:883)');}],
				], frame);},
			function(frame){return thisModule['parse nnt source []'](i('Source', frame), ncore.Frame(frame, {
				'[]' : function(Response, parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler',loc);
					frame._origin = origin;
					frame.Response = Response;
					return ncore.doAll([
						function(frame){return ncore.send(i('parsed',frame), '[]',[i('Response', frame)], frame, '[](node_modules/nnatural-compiler/compile-nnt:887)');}], frame, '*on [](node_modules/nnatural-compiler/compile-nnt:886)');}},frame,'handlers','*node_modules/nnatural-compiler/compile-nnt:885'), 'parse nnt source [](node_modules/nnatural-compiler/compile-nnt:885)');}], frame, 'do in order(node_modules/nnatural-compiler/compile-nnt:882)');}], frame, '*validate [](node_modules/nnatural-compiler/compile-nnt:876)');},
	'get [] errors' : function(Parsed, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Parsed = Parsed;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Parsed', frame), v(["_type"])), "root", frame), function(frame){return ncore.doAll([
				function(frame){return ncore.forEach("Line", ncore.PathRef(i('Parsed', frame), v(["children"])), function(frame){return ncore.doAll([
					function(frame){return thisModule['get [] errors'](i('Line', frame), frame, 'get [] errors(node_modules/nnatural-compiler/compile-nnt:892)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:891)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:891)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:890)');}],
			[require('nnatural/acore')['[] or []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Parsed', frame), v(["_type"])), "free-text", frame), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Parsed', frame), v(["_type"])), "key-value", frame), frame), function(frame){return ncore.doAll([
				function(frame){return ncore.report(frame, 'parse error at []', [ncore.PathRef(i('Parsed', frame), v(["location"]))], 'parse error at [](node_modules/nnatural-compiler/compile-nnt:894)');},
				function(frame){return ncore.forEach("Line", ncore.PathRef(i('Parsed', frame), v(["children"])), function(frame){return ncore.doAll([
					function(frame){return thisModule['get [] errors'](i('Line', frame), frame, 'get [] errors(node_modules/nnatural-compiler/compile-nnt:896)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:895)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:895)');}], frame, '*when [] or [](node_modules/nnatural-compiler/compile-nnt:893)');}],
			[thisModule['[] or [] or []'](require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Parsed', frame), v(["statement", "name"])), "meta", frame), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Parsed', frame), v(["statement", "name"])), "module", frame), require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Parsed', frame), v(["statement", "name"])), "module []", frame), frame), function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/compile-nnt:898)');}], frame, '*when [] or [] or [](node_modules/nnatural-compiler/compile-nnt:897)');}],
			[require('nnatural/acore')['[] eq []'](ncore.PathRef(i('Parsed', frame), v(["_type"])), "block", frame), function(frame){return ncore.doAll([
				function(frame){return ncore.forEach("Line", ncore.PathRef(i('Parsed', frame), v(["children"])), function(frame){return ncore.doAll([
					function(frame){return thisModule['get [] errors'](i('Line', frame), frame, 'get [] errors(node_modules/nnatural-compiler/compile-nnt:901)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:900)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:900)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/compile-nnt:899)');}],
			], frame,'@node_modules/nnatural-compiler/compile-nnt:');}], frame, '*get [] errors(node_modules/nnatural-compiler/compile-nnt:889)');},
	'generate manifest for []' : function(ParsedModule, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.ParsedModule = ParsedModule;
	frame['Manifest'] = require('nnatural/acore')['json'](frame);
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.send(i('Prologs', frame), '[]',[["define action", "define jsaction", "define expression", "define component"]], frame, '[](node_modules/nnatural-compiler/compile-nnt:906)');},
		function(frame){return ncore.forEach("Entity", ncore.PathRef(i('ParsedModule', frame), v(["children"])), function(frame){return ncore.doAll([
			function(frame){return ncore.forEach("Prolog", i('Prologs', frame), function(frame){return ncore.doAll([
				function(frame){return ncore.doIf([
					[require('nnatural/acore')['[] starts with []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), i('Prolog', frame), frame), function(frame){return ncore.doAll([
						function(frame){return ncore.doWhen([
							[ncore.PathRef(i('Entity', frame), v(["statement", "namespace"])),function(frame){return ncore.doAll([
								function(frame){return require('nnatural/acore')['set [] to []'](ncore.PathRef(i('Manifest', frame), v([thisModule['[] trimmed'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), i('Prolog', frame), frame), frame), "type"])), ncore.PathRef(i('Entity', frame), v(["statement", "namespace"])), frame, 'set [] to [](node_modules/nnatural-compiler/compile-nnt:911)');}], frame, '*when [](node_modules/nnatural-compiler/compile-nnt:910)');}],
							[true, function(frame){return ncore.doAll([
								function(frame){return require('nnatural/acore')['set [] to []'](ncore.PathRef(i('Manifest', frame), v([thisModule['[] trimmed'](require('nnatural/acore')['[] after []'](ncore.PathRef(i('Entity', frame), v(["statement", "name"])), i('Prolog', frame), frame), frame), "type"])), require('nnatural/acore')['[] after []'](i('Prolog', frame), "define ", frame), frame, 'set [] to [](node_modules/nnatural-compiler/compile-nnt:913)');}], frame, '*otherwise(node_modules/nnatural-compiler/compile-nnt:912)');}]], frame,'@node_modules/nnatural-compiler/compile-nnt:912');}], frame, '*if [] starts with [](node_modules/nnatural-compiler/compile-nnt:909)');}],
					], frame);}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:908)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:908)');}], frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:907)');}, frame, '*for each [] in [](node_modules/nnatural-compiler/compile-nnt:907)');},
		function(frame){return ncore.report(frame, '[]', [i('Manifest', frame)], '[](node_modules/nnatural-compiler/compile-nnt:924)');}], frame, '*generate manifest for [](node_modules/nnatural-compiler/compile-nnt:903)');},
	'generate resolved name [] namespace []' : function(Name, Namespace, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Name = Name;
	frame.Namespace = Namespace;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.send(ncore.contextRef('entity-resolver', frame), 'generate resolved name [] namespace []',[i('Name', frame),i('Namespace', frame)], frame, 'generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:927)');}], frame, '*generate resolved name [] namespace [](node_modules/nnatural-compiler/compile-nnt:926)');},
	'write module prolog for []' : function(Module, parent, loc){
	var frame = ncore.Frame(parent,null,local,'action',loc);
	frame.Module = Module;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return require('./writer')['writeln []']("var ncore = require('nnatural/ncore');", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:930)');},
		function(frame){return require('./writer')['write []']("function i(id,frame){if(frame[id]!== undefined){return frame[id];}ncore.getContextFrame(frame)[id] = ", frame, 'write [](node_modules/nnatural-compiler/compile-nnt:931)');},
		function(frame){return require('./writer')['write []'](require('nnatural/acore')['[] default []'](ncore.contextRef('DefaultComponent', frame), "require('nnatural/acore')['json']", frame), frame, 'write [](node_modules/nnatural-compiler/compile-nnt:932)');},
		function(frame){return require('./writer')['writeln []']("();return frame[id];}", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:933)');},
		function(frame){return require('./writer')['writeln []'](ncore.template(["var r=",require('nnatural/acore')['[] default []'](ncore.contextRef('DefaultReferenceFunction', frame), "function(ref){return ref;}", frame),";"]), frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:934)');},
		function(frame){return require('./writer')['writeln []']("var v=ncore.nValue;", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:937)');},
		function(frame){return require('./writer')['writeln []']("var e=require('nnatural/context').emit;", frame, 'writeln [](node_modules/nnatural-compiler/compile-nnt:938)');},
		function(frame){return thisModule['generate module declaration []'](ncore.PathRef(i('Module', frame), v(["children", "0"])), frame, 'generate module declaration [](node_modules/nnatural-compiler/compile-nnt:939)');}], frame, '*write module prolog for [](node_modules/nnatural-compiler/compile-nnt:929)');}};
module.exports = thisModule;
