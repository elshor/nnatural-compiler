//Wed, 13 Apr 2016 06:10:57 GMT
var ncore = require('nnatural/ncore');
function i(id,frame){if(frame[id]!== undefined){return frame[id];}ncore.getContextFrame(frame)[id] = 
require('nnatural/acore')['json']();return frame[id];}var v=ncore.nValue;
var local = {
	'isFrame' : true}
;
var thisModule = {
	'library of entity [] in []' : function(Name, Manifests, frame){
	return thisModule['library of entity [] in [] at []'](Name, Manifests, require('nnatural/acore')['[] minus []'](ncore.PathRef(Manifests, v(["length"])), 1, frame), frame);},
	'library of entity [] in [] at []' : function(Name, Manifests, N, frame){
	return require('nnatural/acore')['[] is empty'](Manifests, frame)? null : require('nnatural/acore')['[] lt []'](N, 0, frame)? null : v(ncore.PathRef(Manifests, v([N, "1", Name])))? ncore.PathRef(Manifests, v([N, "0"])) : thisModule['library of entity [] in [] at []'](Name, Manifests, require('nnatural/acore')['[] minus []'](N, 1, frame), frame);},
	'entity resolver' : function(parent){
	var frame = ncore.Frame(null,null,local,'component');
	frame.this = frame;
	frame['Manifests'] = require('nnatural/acore')['json'](frame);
	frame._events['push library []'] = function(Id, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.Id = Id;
		return ncore.doAll([
			function(frame){return ncore.doInOrder([
				function(frame){return ncore.doIf([
					[thisModule['resolve require [] from []'](v([i('Id', frame),".manifest"]).join(''), ncore.contextRef('file-name', frame), frame), function(frame){return ncore.doAll([
						function(frame){return require('nnatural/file')['load file []'](thisModule['resolve require [] from []'](v([i('Id', frame),".manifest"]).join(''), ncore.contextRef('file-name', frame), frame), ncore.Frame(frame, {
							'[]' : function(Text, parent, reportTo, origin, loc){
								var frame = ncore.Frame(reportTo, null, parent,'message-handler');
								frame._origin = origin;
								frame._loc = loc;
								frame.Text = Text;
								return ncore.doAll([
									function(frame){return ncore.send(i('Manifest', frame), '[]',[require('nnatural/acore')['[] as json'](i('Text', frame), frame)], frame, '[](node_modules/nnatural-compiler/cmd:34)');}], frame, '*on [](node_modules/nnatural-compiler/cmd:33)');}},frame), 'load file [](node_modules/nnatural-compiler/cmd:32)');}], frame, '*if resolve require [] from [](node_modules/nnatural-compiler/cmd:31)');}],
					], frame);},
				function(frame){return ncore.doIf([
					[require('nnatural/acore')['not []'](i('Manifest', frame), frame), function(frame){return ncore.doAll([
						function(frame){return ncore.doInOrder([
							function(frame){return ncore.doIf([
								[thisModule['resolve require [] from []'](v([i('Id', frame),".nnt"]).join(''), ncore.contextRef('file-name', frame), frame), function(frame){return ncore.doAll([
									function(frame){return require('nnatural/file')['load file []'](thisModule['resolve require [] from []'](v([i('Id', frame),".nnt"]).join(''), ncore.contextRef('file-name', frame), frame), ncore.Frame(frame, {
										'[]' : function(Nnt, parent, reportTo, origin, loc){
											var frame = ncore.Frame(reportTo, null, parent,'message-handler');
											frame._origin = origin;
											frame._loc = loc;
											frame.Nnt = Nnt;
											return ncore.doAll([
												function(frame){return require('./compile-nnt')['parse nnt source []'](i('Nnt', frame), ncore.Frame(frame, {
													'[]' : function(Parsed, parent, reportTo, origin, loc){
														var frame = ncore.Frame(reportTo, null, parent,'message-handler');
														frame._origin = origin;
														frame._loc = loc;
														frame.Parsed = Parsed;
														return ncore.doAll([
															function(frame){return require('./compile-nnt')['generate manifest for []'](i('Parsed', frame), ncore.Frame(frame, {
																'[]' : function(NewManifest, parent, reportTo, origin, loc){
																	var frame = ncore.Frame(reportTo, null, parent,'message-handler');
																	frame._origin = origin;
																	frame._loc = loc;
																	frame.NewManifest = NewManifest;
																	return ncore.doAll([
																		function(frame){return ncore.send(i('Manifest', frame), '[]',[i('NewManifest', frame)], frame, '[](node_modules/nnatural-compiler/cmd:44)');}], frame, '*on [](node_modules/nnatural-compiler/cmd:43)');}},frame), 'generate manifest for [](node_modules/nnatural-compiler/cmd:42)');}], frame, '*on [](node_modules/nnatural-compiler/cmd:41)');}},frame), 'parse nnt source [](node_modules/nnatural-compiler/cmd:40)');}], frame, '*on [](node_modules/nnatural-compiler/cmd:39)');}},frame), 'load file [](node_modules/nnatural-compiler/cmd:38)');}], frame, '*if resolve require [] from [](node_modules/nnatural-compiler/cmd:37)');}],
								], frame);}], frame, 'do in order(node_modules/nnatural-compiler/cmd:36)');}], frame, '*if not [](node_modules/nnatural-compiler/cmd:35)');}],
					], frame);},
				function(frame){return ncore.doIf([
					[i('Manifest', frame),function(frame){return ncore.doAll([
						function(frame){return ncore.send(i('this',frame), 'push manifest [] named []',[i('Manifest', frame),i('Id', frame)], frame, 'push manifest [] named [](node_modules/nnatural-compiler/cmd:46)');}], frame, '*if [](node_modules/nnatural-compiler/cmd:45)');}],
					[true, function(frame){return ncore.doAll([
						function(frame){return require('nnatural/acore')['log []'](v(["cannot resolve library '",i('Id', frame),"' from ",require('path').resolve(v(ncore.contextRef('file-name',frame)))]).join(''), frame, 'log [](node_modules/nnatural-compiler/cmd:48)');},
						function(frame){return ncore.report(frame, 'error []', [v(["Cannot find library ",i('Id', frame)]).join('')], 'error [](node_modules/nnatural-compiler/cmd:49)');}], frame, '*else(node_modules/nnatural-compiler/cmd:47)');}]], frame);}], frame, 'do in order(node_modules/nnatural-compiler/cmd:30)');}], frame, '*on push library [](node_modules/nnatural-compiler/cmd:29)');};
	frame._events['push manifest [] named []'] = function(Manifest, Name, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.Manifest = Manifest;
		frame.Name = Name;
		return ncore.doAll([
			function(frame){return require('nnatural/acore')['insert [] into component []']([i('Name', frame), i('Manifest', frame)], i('Manifests', frame), frame, 'insert [] into component [](node_modules/nnatural-compiler/cmd:51)');}], frame, '*on push manifest [] named [](node_modules/nnatural-compiler/cmd:50)');};
	frame._events['generate resolved name [] namespace []'] = function(Name, Namespace, parent, reportTo, origin, loc){
		var frame = ncore.Frame(reportTo, null, parent,'message-handler');
		frame._origin = origin;
		frame._loc = loc;
		frame.Name = Name;
		frame.Namespace = Namespace;
		return ncore.doAll([
			function(frame){return ncore.doIf([
				[require('nnatural/acore')['[] or []'](require('nnatural/acore')['[] eq []'](i('Name', frame), "", frame), require('nnatural/acore')['not []'](i('Name', frame), frame), frame), function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['log []'](v(["Error - got empty name at ",require('nnatural/acore')['[] as text'](ncore.contextRef('Statement', frame), frame)]).join(''), frame, 'log [](node_modules/nnatural-compiler/cmd:54)');},
					function(frame){return require('nnatural/acore')['show stack trace'](frame, 'show stack trace(node_modules/nnatural-compiler/cmd:55)');}], frame, '*if [] or [](node_modules/nnatural-compiler/cmd:53)');}],
				], frame);},
			function(frame){return ncore.send(i('Library', frame), '[]',[require('nnatural/acore')['[] default []'](i('Namespace', frame), thisModule['library of entity [] in []'](i('Name', frame), i('Manifests', frame), frame), frame)], frame, '[](node_modules/nnatural-compiler/cmd:56)');},
			function(frame){return ncore.doWhen([
				[require('nnatural/acore')['[] eq []'](i('Library', frame), "this", frame), function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](v(["thisModule['",i('Name', frame),"']"]).join(''), frame, 'write [](node_modules/nnatural-compiler/cmd:58)');}], frame, '*when [] eq [](node_modules/nnatural-compiler/cmd:57)');}],
				[i('Library', frame),function(frame){return ncore.doAll([
					function(frame){return require('./writer')['write []'](v(["require('",i('Library', frame),"')['",i('Name', frame),"']"]).join(''), frame, 'write [](node_modules/nnatural-compiler/cmd:62)');}], frame, '*when [](node_modules/nnatural-compiler/cmd:60)');}],
				[true, function(frame){return ncore.doAll([
					function(frame){return require('nnatural/acore')['log []'](v(["Cannot resolve entity '",i('Name', frame),"' at ",ncore.contextRef('file-name', frame),":",ncore.PathRef(ncore.contextRef('Statement', frame), v(["location", "start", "line"])),":",ncore.PathRef(ncore.contextRef('Statement', frame), v(["location", "start", "column"]))]).join(''), frame, 'log [](node_modules/nnatural-compiler/cmd:64)');},
					function(frame){return ncore.report(frame, 'entity [] not resolved at []', [i('Name', frame), ncore.PathRef(ncore.contextRef('Statement', frame), v(["location"]))], 'entity [] not resolved at [](node_modules/nnatural-compiler/cmd:65)');}], frame, '*otherwise(node_modules/nnatural-compiler/cmd:63)');}]], frame);}], frame, '*on generate resolved name [] namespace [](node_modules/nnatural-compiler/cmd:52)');};
	return frame;},
	'resolve require [] from []' : function(Id, FilePath, frame){
	return function(Id){
			try{
				return require('resolve').sync(v(Id),{basedir: require('path').dirname(require('path').resolve(v(FilePath)))});
			}catch(e){
				return null;
			}
		}(Id)
	;},
	'main' : function(parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame['parsed'] = require('nnatural/acore')['json'](frame);
	frame['input'] = require('nnatural/acore')['json'](frame);
	frame['the file-name'] = require('nnatural/acore')['json'](frame);
	frame['the stdout'] = require('./writer')['writer'](frame);
	frame['the entity-resolver'] = thisModule['entity resolver'](frame);
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return ncore.doWhen([
			[require('nnatural/acore')['[] or []'](require('nnatural/command-line-arguments')['argument []']("help", frame), process.argv.length < 3, frame), function(frame){return ncore.doAll([
				function(frame){return require('nnatural/acore')['log []']("Usage: nnt modulename [options]", frame, 'log [](node_modules/nnatural-compiler/cmd:85)');},
				function(frame){return require('nnatural/acore')['log []']("Options:", frame, 'log [](node_modules/nnatural-compiler/cmd:86)');},
				function(frame){return require('nnatural/acore')['log []']("\t-help\tshow this message", frame, 'log [](node_modules/nnatural-compiler/cmd:87)');},
				function(frame){return require('nnatural/acore')['log []']("\t-parse\toutput parsed source file as json", frame, 'log [](node_modules/nnatural-compiler/cmd:88)');}], frame, '*when [] or [](node_modules/nnatural-compiler/cmd:84)');}],
			[true, function(frame){return ncore.doAll([
				function(frame){return ncore.doInOrder([
					function(frame){return ncore.send(ncore.contextRef('file-name', frame), '[]',[process.argv[2]], frame, '[](node_modules/nnatural-compiler/cmd:91)');},
					function(frame){return require('nnatural/file')['load file [] into []'](v([ncore.contextRef('file-name', frame),".nnt"]).join(''), i('input',frame), ncore.Frame(frame, {
						'error []' : function(Error, parent, reportTo, origin, loc){
							var frame = ncore.Frame(reportTo, null, parent,'message-handler');
							frame._origin = origin;
							frame._loc = loc;
							frame.Error = Error;
							return ncore.doAll([
								function(frame){return require('nnatural/acore')['log []'](v(["Error opening file ",ncore.contextRef('file-name', frame)," - ",i('Error', frame)]).join(''), frame, 'log [](node_modules/nnatural-compiler/cmd:94)');},
								function(frame){return ncore.exitDoInOrder(frame, 'exit do in order(node_modules/nnatural-compiler/cmd:95)');}], frame, '*on error [](node_modules/nnatural-compiler/cmd:93)');}},frame), 'load file [] into [](node_modules/nnatural-compiler/cmd:92)');},
					function(frame){return require('./compile-nnt')['parse nnt source []'](i('input',frame), ncore.Frame(frame, {
						'[]' : function(Parsed, parent, reportTo, origin, loc){
							var frame = ncore.Frame(reportTo, null, parent,'message-handler');
							frame._origin = origin;
							frame._loc = loc;
							frame.Parsed = Parsed;
							return ncore.doAll([
								function(frame){return ncore.send(i('parsed',frame), '[]',[i('Parsed', frame)], frame, '[](node_modules/nnatural-compiler/cmd:98)');}], frame, '*on [](node_modules/nnatural-compiler/cmd:97)');}},frame), 'parse nnt source [](node_modules/nnatural-compiler/cmd:96)');},
					function(frame){return ncore.doIf([
						[require('nnatural/command-line-arguments')['argument []']("parse", frame), function(frame){return ncore.doAll([
							function(frame){return require('nnatural/file')['save [] as file named []'](require('nnatural/acore')['[] as text'](i('parsed',frame), frame), v([ncore.contextRef('file-name', frame),".json"]).join(''), frame, 'save [] as file named [](node_modules/nnatural-compiler/cmd:100)');}], frame, '*if argument [](node_modules/nnatural-compiler/cmd:99)');}],
						], frame);},
					function(frame){return ncore.send(ncore.contextRef('stdout', frame), 'open []',[v([ncore.contextRef('file-name', frame),".js"]).join('')], frame, 'open [](node_modules/nnatural-compiler/cmd:101)');},
					function(frame){return require('./writer')['writeln []'](v(["//",require('nnatural/acore')['now as text'](frame)]).join(''), frame, 'writeln [](node_modules/nnatural-compiler/cmd:102)');},
					function(frame){return ncore.send(ncore.contextRef('entity-resolver', frame), 'push library []',["nnatural/acore"], ncore.Frame(frame, {
						'error []' : function(Error, parent, reportTo, origin, loc){
							var frame = ncore.Frame(reportTo, null, parent,'message-handler');
							frame._origin = origin;
							frame._loc = loc;
							frame.Error = Error;
							return ncore.doAll([
								function(frame){return require('nnatural/acore')['log []']("Cannot load library 'nnatural/acore'. Exiting without generating module", frame, 'log [](node_modules/nnatural-compiler/cmd:105)');},
								function(frame){return ncore.exitDoInOrder(frame, 'exit do in order(node_modules/nnatural-compiler/cmd:106)');}], frame, '*on error [](node_modules/nnatural-compiler/cmd:104)');}},frame), 'push library [](node_modules/nnatural-compiler/cmd:103)');},
					function(frame){return require('./compile-nnt')['generate manifest for []'](i('parsed',frame), ncore.Frame(frame, {
						'[]' : function(Manifest, parent, reportTo, origin, loc){
							var frame = ncore.Frame(reportTo, null, parent,'message-handler');
							frame._origin = origin;
							frame._loc = loc;
							frame.Manifest = Manifest;
							return ncore.doAll([
								function(frame){return require('nnatural/file')['save [] as file named []'](require('nnatural/acore')['[] as text'](i('Manifest', frame), frame), v([ncore.contextRef('file-name', frame),".manifest"]).join(''), frame, 'save [] as file named [](node_modules/nnatural-compiler/cmd:109)');}], frame, '*on [](node_modules/nnatural-compiler/cmd:108)');}},frame), 'generate manifest for [](node_modules/nnatural-compiler/cmd:107)');},
					function(frame){return require('./compile-nnt')['generate module []'](i('parsed',frame), ncore.Frame(frame, {
						'nnt [] named [] html name [] with body [] source []' : function(Type, Name, HtmlName, Body, Source, parent, reportTo, origin, loc){
							var frame = ncore.Frame(reportTo, null, parent,'message-handler');
							frame._origin = origin;
							frame._loc = loc;
							frame.Type = Type;
							frame.Name = Name;
							frame.HtmlName = HtmlName;
							frame.Body = Body;
							frame.Source = Source;
							return ncore.doAll([
								function(frame){return require('nnatural/acore')['nothing'](frame, 'nothing(node_modules/nnatural-compiler/cmd:112)');}], frame, '*on nnt [] named [] html name [] with body [] source [](node_modules/nnatural-compiler/cmd:111)');},
						'module []' : function(Name, parent, reportTo, origin, loc){
							var frame = ncore.Frame(reportTo, null, parent,'message-handler');
							frame._origin = origin;
							frame._loc = loc;
							frame.Name = Name;
							return ncore.doAll([
								function(frame){return require('nnatural/acore')['log []'](v(["generating module ",i('Name', frame)]).join(''), frame, 'log [](node_modules/nnatural-compiler/cmd:114)');}], frame, '*on module [](node_modules/nnatural-compiler/cmd:113)');}},frame), 'generate module [](node_modules/nnatural-compiler/cmd:110)');},
					function(frame){return ncore.send(ncore.contextRef('stdout', frame), 'close',[], frame, 'close(node_modules/nnatural-compiler/cmd:115)');}], frame, 'do in order(node_modules/nnatural-compiler/cmd:90)');}], frame, '*otherwise(node_modules/nnatural-compiler/cmd:89)');}]], frame);}], frame, '*main(node_modules/nnatural-compiler/cmd:77)');}};
module.exports = thisModule;
ncore.dispatch(module.exports.main,[ncore.isNEngineContext()? null : 
require('nnatural/acore')['shell'](null),null]);